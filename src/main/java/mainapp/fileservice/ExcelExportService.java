package mainapp.fileservice;

import mainapp.domain.loan.Loan;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class ExcelExportService {

    final static String FILENAME_PREFIX = "Loans_Export";

    public void exportLoansToExcel(List<Loan> loansToExport) throws IOException {
        Workbook exportWk = new XSSFWorkbook();
        Sheet sheet = exportWk.createSheet("export");
        String[] headerNames = new String[]{"Id","Lender","Borrower","Start date", "Expiry Date", "Auto-extend", "Initial Amount", "Currency", "Loan type"};
        createHeaderRow(sheet,headerNames);

        int rowCounter = 1;
        for (Loan loan : loansToExport) {
            Row row = sheet.createRow(rowCounter);
            row.createCell(0).setCellValue(loan.getId()==null ?  "" : loan.getId().toString());
            row.createCell(1).setCellValue(loan.getLender().getName());
            row.createCell(2).setCellValue(loan.getBorrower().getName());
            row.createCell(3).setCellValue(loan.getStartDate());
            row.createCell(4).setCellValue(loan.getEndDate());
            row.createCell(5).setCellValue(loan.isAutoExtend());
            row.createCell(6).setCellValue(loan.getLoanAmount().doubleValue());
            row.createCell(7).setCellValue(loan.getCurrency().getCurrencyCode());
            row.createCell(8).setCellValue(loan.getLoanType().name());
            rowCounter++;
        }
        saveAndShow(exportWk);
    }

    private void saveAndShow(Workbook exportWk) throws IOException {
        String nameOfFile = FILENAME_PREFIX + "_" + LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddhhmmss"));
        saveFile(exportWk, nameOfFile);
        openFile(defineDestinationPath(nameOfFile));
    }

    private void createHeaderRow(Sheet sheet, String[] headerNames) {
        Row header = sheet.createRow(0);
        for (int i=0; i<headerNames.length; i++) {
            Cell headerCell = header.createCell(i);
            headerCell.setCellValue(headerNames[i]);
        }
    }

    private void saveFile(Workbook workbook, String fileName) {
        String path = defineDestinationPath(fileName);
        try {
            FileOutputStream outputStream = new FileOutputStream(path);
            workbook.write(outputStream);
            outputStream.close();
            workbook.close();
        } catch (Exception e ) {
            e.printStackTrace();
        }
    }


    private void openFile(String path) throws IOException {
        Runtime r= Runtime.getRuntime();
        r.exec("cmd /c " + path);
    }

    private String defineDestinationPath(String filename){
        String userSpecific = System.getProperty("user.name");
        return  "C:\\Users\\" + userSpecific + "\\Downloads\\" + filename + ".xlsx";
    }


}
