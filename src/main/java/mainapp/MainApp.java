package mainapp;

import mainapp.ui.FXUIApp;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@SpringBootApplication
public class MainApp {

    public static void main(String[] args) {

        FXUIApp.main(args);

    }

}
