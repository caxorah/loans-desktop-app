package mainapp.database.config;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class RetryMethodExecutor {

    @Around("@annotation(retry)")
    public Object execute(ProceedingJoinPoint proceedingJoinPoint, Retry retry) throws Throwable {
        int currentAttempt = 0;
        Throwable throwable;
        do {
            currentAttempt++;
            try {
                return proceedingJoinPoint.proceed();
            } catch (Throwable t) {
                throwable = t;
                int MILLIS_WAIT_BETWEEN_ATTEMPTS = 200;
                Thread.sleep(MILLIS_WAIT_BETWEEN_ATTEMPTS);
            }
        } while (currentAttempt < retry.attempts());
        throw throwable;
    }
}
