package mainapp.database.attachment;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "attachments")
@Data
public class AttachmentEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "file")
    private byte[] file;

    @Column(name = "file_name")
    private String fileName;

    @Column(name = "description")
    private String description;

    @Column
    private Long parentId;

}
