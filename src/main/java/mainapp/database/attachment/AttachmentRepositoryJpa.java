package mainapp.database.attachment;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AttachmentRepositoryJpa extends JpaRepository<AttachmentEntity,Integer> {

    List<AttachmentEntity> findByParentId(Long parentId);
}
