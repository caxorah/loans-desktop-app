package mainapp.database.attachment;

import lombok.RequiredArgsConstructor;
import mainapp.domain.attachment.Attachment;
import mainapp.domain.attachment.AttachmentRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Repository
@RequiredArgsConstructor
public class AttachmentRepositoryAdapter implements AttachmentRepository {

    private final AttachmentRepositoryJpa attachmentRepositoryJpa;
    private final AttachmentMapper mapper;


    @Override
    public Attachment save(Attachment attachment) {
        AttachmentEntity entity = attachmentRepositoryJpa.save(mapper.toEntity(attachment));
        return mapper.toDomain(entity);
    }

    @Override
    public List<Attachment> findByParentId(Long parentObjectId) {
        List<AttachmentEntity> attachments = attachmentRepositoryJpa.findByParentId(parentObjectId);
        return attachments.stream()
                .map(mapper::toDomain)
                .collect(Collectors.toList());
    }

    @Override
    public void delete(Attachment attachment) {
        Integer id = attachment.getId();
        attachmentRepositoryJpa.deleteById(id);
    }
}
