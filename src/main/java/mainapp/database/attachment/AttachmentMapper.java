package mainapp.database.attachment;

import mainapp.domain.attachment.Attachment;
import org.springframework.stereotype.Service;

@Service
public class AttachmentMapper {

    public Attachment toDomain(AttachmentEntity entity) {
        Attachment domain = new Attachment();
        domain.setId(entity.getId());
        domain.setFileName(entity.getFileName());
        domain.setFile(entity.getFile());
        domain.setDescription(entity.getDescription());
        domain.setParentId(entity.getParentId());
        return domain;
    }

    public AttachmentEntity toEntity(Attachment domain) {
        AttachmentEntity entity = new AttachmentEntity();
        entity.setId(domain.getId());
        entity.setFileName(domain.getFileName());
        entity.setFile(domain.getFile());
        entity.setDescription(domain.getDescription());
        entity.setParentId(domain.getParentId());
        return entity;
    }

}
