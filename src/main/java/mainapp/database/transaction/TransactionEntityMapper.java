package mainapp.database.transaction;

import lombok.RequiredArgsConstructor;
import mainapp.domain.transaction.Transaction;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class TransactionEntityMapper {

    public Transaction toDomain(TransactionEntity transactionEntity) {
        return new Transaction(
                transactionEntity.getId(),
                transactionEntity.getDate(),
                transactionEntity.getAmount(),
                transactionEntity.getScope(),
                transactionEntity.getDescription(),
                transactionEntity.getRate(),
                transactionEntity.getLoanId()
        );
    }

    public TransactionEntity toEntity(Transaction transaction) {
         TransactionEntity entity = new TransactionEntity(
                 transaction.getId(),
                 transaction.getDate(),
                 transaction.getAmount(),
                 transaction.getScope(),
                 transaction.getDescription(),
                 transaction.getRate()
        );
         entity.setLoanId(transaction.getLoanId());
         return entity;
    }

}
