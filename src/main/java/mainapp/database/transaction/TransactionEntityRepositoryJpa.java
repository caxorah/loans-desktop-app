package mainapp.database.transaction;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TransactionEntityRepositoryJpa extends JpaRepository<TransactionEntity, Long> {

    List<TransactionEntity> findAllByLoanId(Long loanId);

    void deleteAllByLoanId(Long loanId);

}
