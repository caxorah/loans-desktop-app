package mainapp.database.transaction;

import lombok.RequiredArgsConstructor;
import mainapp.domain.transaction.Transaction;
import mainapp.domain.transaction.TransactionRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Repository
@RequiredArgsConstructor
public class TransactionRepositoryAdapter implements TransactionRepository {

    private final TransactionEntityRepositoryJpa repositoryJpa;
    private final TransactionEntityMapper mapper;

    @Override
    public List<Transaction> findAllByLoanId(Long loanId) {
        List<TransactionEntity> transactions = repositoryJpa.findAllByLoanId(loanId);
        return transactions.stream()
                .map(mapper::toDomain)
                .collect(Collectors.toList());
    }

    @Override
    public Transaction save(Transaction transaction) {
        TransactionEntity saved = repositoryJpa.save(mapper.toEntity(transaction));
        return mapper.toDomain(saved);
    }

    @Override
    public void deleteById(Long id) {
        repositoryJpa.deleteById(id);
    }

    @Override
    public void deleteAllByLoanId(Long loanId) {
        repositoryJpa.deleteAllByLoanId(loanId);
    }
}
