package mainapp.database.transaction;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import mainapp.domain.transaction.TransactionScope;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Entity
@Table(name = "transactions")
public class TransactionEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private LocalDate date;
    private BigDecimal amount;
    @Enumerated
    private TransactionScope scope;
    private String description;
    private double rate;
    private Long loanId;

    public TransactionEntity(Long id, LocalDate date, BigDecimal amount, TransactionScope scope, String description, double rate) {
        this.id = id;
        this.date = date;
        this.amount = amount;
        this.scope = scope;
        this.description = description;
        this.rate = rate;
    }

    public void setLoanId(Long loanId) {
        this.loanId = loanId;
    }

    @Override
    public String toString() {
        return "TransactionEntity{" +
                "id=" + id +
                ", date=" + date +
                ", amount=" + amount +
                ", scope=" + scope +
                ", description='" + description + '\'' +
                ", rate=" + rate +
                ", loanEntity=" + loanId + "}";
    }
}
