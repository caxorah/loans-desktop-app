package mainapp.database.company;

import mainapp.database.config.Retry;
import mainapp.domain.company.Company;
import mainapp.domain.company.CompanyRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Repository
public class CompanyRepositoryAdapter implements CompanyRepository {

    private final CompanyRepositoryJpa companyRepositoryJpa;
    private final CompanyMapper companyMapper;

    public CompanyRepositoryAdapter(CompanyRepositoryJpa companyRepositoryJpa, CompanyMapper companyMapper) {
        this.companyRepositoryJpa = companyRepositoryJpa;
        this.companyMapper = companyMapper;
    }

    @Override
    @Retry
    public List<Company> getAll() {
        List<CompanyEntity> companyEntityList = companyRepositoryJpa.findAll();
        return companyEntityList.stream().map(companyMapper::toDomain).collect(Collectors.toList());
    }

    @Override
    @Retry
    public Company getCompany(int entityNumber) {
        CompanyEntity foundEntity = companyRepositoryJpa.getById(entityNumber);
        return companyMapper.toDomain(foundEntity);
    }

    @Override
    @Retry
    public Company save(Company company) {
        CompanyEntity toSave = companyMapper.toEntity(company);
        CompanyEntity afterSave = companyRepositoryJpa.save(toSave);
        return companyMapper.toDomain(afterSave);
    }
}
