package mainapp.database.company;

import mainapp.domain.company.Company;
import org.springframework.stereotype.Service;

@Service
public class CompanyMapper {

    public Company toDomain(CompanyEntity companyEntity) {
        return new Company(
                companyEntity.getEntityNumber(),
                companyEntity.getName()
        );
    }

    public CompanyEntity toEntity(Company company) {
        CompanyEntity companyEntity = new CompanyEntity();
        companyEntity.setEntityNumber(company.getEntityNumber());
        companyEntity.setName(company.getName());
        return companyEntity;
    }
}
