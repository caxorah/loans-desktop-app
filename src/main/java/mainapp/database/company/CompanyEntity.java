package mainapp.database.company;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@NoArgsConstructor
@Data
@Entity
@Table(name = "companies")
public class CompanyEntity {

    @Id
    @Column(name = "entity_number")
    private Integer entityNumber;
    private String name;

}
