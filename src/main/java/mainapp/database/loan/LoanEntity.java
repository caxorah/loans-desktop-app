package mainapp.database.loan;

import lombok.Getter;
import lombok.Setter;
import mainapp.database.company.CompanyEntity;
import mainapp.database.transaction.TransactionEntity;
import mainapp.domain.interests.InterestsCalculationPolicy;
import mainapp.domain.loan.LoanType;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "loans")
public class LoanEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    private CompanyEntity lender;

    @ManyToOne(fetch = FetchType.EAGER)
    private CompanyEntity borrower;

    @Column(name = "start_date")
    private LocalDate startDate;

    @Column(name = "end_date")
    private LocalDate endDate;

    private boolean autoExtend;

    private String currency;

    @Enumerated
    @Column(name = "interests_calculation_policy")
    private InterestsCalculationPolicy interestsCalculationPolicy;

    private BigDecimal loanAmount;

    @OneToMany(cascade = {CascadeType.ALL}, mappedBy = "loanId", fetch = FetchType.LAZY)
    private List<TransactionEntity> transactions = new ArrayList<>();

    @Enumerated(EnumType.STRING)
    private LoanType loanType;

    private String comment;


    @Override
    public String toString() {
        return "LoanEntity{" +
                "id='" + id + '\'' +
                ", lender=" + lender +
                ", borrower=" + borrower +
                ", amount=" + loanAmount +
                '}';
    }
}
