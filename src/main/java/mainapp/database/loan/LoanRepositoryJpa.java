package mainapp.database.loan;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LoanRepositoryJpa extends JpaRepository<LoanEntity, Long> {
}
