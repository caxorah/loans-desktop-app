package mainapp.database.loan;

import mainapp.database.config.Retry;
import mainapp.domain.loan.LoanRepository;
import mainapp.domain.loan.Loan;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class LoanRepositoryAdapter implements LoanRepository {

    private final LoanRepositoryJpa loanRepositoryJpa;
    private final LoanMapper loanMapper;

    public LoanRepositoryAdapter(LoanRepositoryJpa loanRepositoryJpa, LoanMapper loanMapper) {
        this.loanRepositoryJpa = loanRepositoryJpa;
        this.loanMapper = loanMapper;
    }

    @Override
    @Retry
    public Loan save(Loan loan) {
        LoanEntity loanEntity = loanMapper.toEntity(loan);
        LoanEntity saved = loanRepositoryJpa.saveAndFlush(loanEntity);
        return loanMapper.toDomain(saved);
    }

    @Override
    @Retry
    public List<Loan> getAllLoans() {
        List<LoanEntity> allLoans = loanRepositoryJpa.findAll();
        return allLoans.stream().map(loanMapper::toDomain).collect(Collectors.toList());
    }

    @Override
    @Retry
    public Optional<Loan> getById(Long id) {
        Optional<LoanEntity> found = loanRepositoryJpa.findById(id);
        return Optional.ofNullable(loanMapper.toDomain(found.get()));
    }

    @Override
    @Retry
    public void delete(Loan loan) {
        LoanEntity loanEntity = loanMapper.toEntity(loan);
        loanRepositoryJpa.delete(loanEntity);
    }
}
