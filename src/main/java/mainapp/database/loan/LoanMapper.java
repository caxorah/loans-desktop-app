package mainapp.database.loan;

import lombok.RequiredArgsConstructor;
import mainapp.database.company.CompanyMapper;
import mainapp.database.transaction.TransactionEntityMapper;
import mainapp.domain.loan.Loan;
import mainapp.domain.loan.LoanNameGenerator;
import org.springframework.stereotype.Component;

import java.util.Currency;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class LoanMapper {

    private final CompanyMapper companyMapper;
    private final LoanNameGenerator loanNameGenerator;
    private final TransactionEntityMapper transactionEntityMapper;

    public LoanEntity toEntity(Loan loan) {
        LoanEntity loanEntity = new LoanEntity();
        loanEntity.setLender(companyMapper.toEntity(loan.getLender()));
        loanEntity.setBorrower(companyMapper.toEntity(loan.getBorrower()));
        loanEntity.setAutoExtend(loan.isAutoExtend());
        loanEntity.setLoanAmount(loan.getLoanAmount());
        loanEntity.setLoanType(loan.getLoanType());
        loanEntity.setEndDate(loan.getEndDate());
        loanEntity.setStartDate(loan.getStartDate());
        loanEntity.setCurrency(loan.getCurrency().getCurrencyCode());
        loanEntity.setInterestsCalculationPolicy(loan.getInterestsCalculationPolicy());
        loanEntity.setId(loan.getId());
        loanEntity.setComment(loan.getComment());
        return loanEntity;
    }

    public Loan toDomain(LoanEntity loanEntity) {
        Loan loan = Loan.builder()
                .lender(companyMapper.toDomain(loanEntity.getLender()))
                .borrower(companyMapper.toDomain(loanEntity.getBorrower()))
                .currency(Currency.getInstance(loanEntity.getCurrency()))
                .loanType(loanEntity.getLoanType())
                .autoExtend(loanEntity.isAutoExtend())
                .endDate(loanEntity.getEndDate())
                .loanAmount(loanEntity.getLoanAmount())
                .interestsCalculationPolicy(loanEntity.getInterestsCalculationPolicy())
                .startDate(loanEntity.getStartDate())
                .transactions(loanEntity.getTransactions().stream().map(transactionEntityMapper::toDomain).collect(Collectors.toList()))
                .id(loanEntity.getId())
                .comment(loanEntity.getComment())
                .build();
        loan.setName(loanNameGenerator.getNameOf(loan));
        return loan;
    }

}
