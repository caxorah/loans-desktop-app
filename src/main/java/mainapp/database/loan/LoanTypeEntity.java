package mainapp.database.loan;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
@Getter
@Entity
@Table(name = "loan_types")
public class LoanTypeEntity {

    @Id
    private String name;
    private String lenderAccount;
    private String borrowerAccount;

}
