package mainapp.domain.company;

import java.util.List;

public interface CompanyRepository {

    List<Company> getAll();

    Company getCompany(int entityNumber);

    Company save(Company company);

}
