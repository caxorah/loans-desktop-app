package mainapp.domain.company;

import java.util.List;

public class CompanyService {

    private final CompanyRepository companyRepository;

    public CompanyService(CompanyRepository companyRepository) {
        this.companyRepository = companyRepository;
    }

    public Company getCompanyByNumber(int entityNumber) {
        return companyRepository.getCompany(entityNumber);
    }

    public List<Company> getAll() {
        return companyRepository.getAll();
    }

    public void save(Company company) {
        companyRepository.save(company);
    }
}
