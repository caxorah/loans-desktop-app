package mainapp.domain;

import java.time.LocalDate;

public interface PeriodConverter {

    String convertDateToQ(LocalDate date);

}
