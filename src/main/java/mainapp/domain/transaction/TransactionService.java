package mainapp.domain.transaction;

import lombok.RequiredArgsConstructor;

import java.util.List;

@RequiredArgsConstructor
public class TransactionService {

    private final TransactionRepository transactionRepository;

    public List<Transaction> getLoanTransactions(Long loanId){
        return transactionRepository.findAllByLoanId(loanId);
    }

    public Transaction save(Transaction transaction) {
        return transactionRepository.save(transaction);
    }

    public void delete(Long id) {
        transactionRepository.deleteById(id);
    }

    public void deleteAllLoanTransactions(Long loanId){
        transactionRepository.deleteAllByLoanId(loanId);
    }

}
