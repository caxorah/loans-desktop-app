package mainapp.domain.transaction;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

@Builder
@Data
@AllArgsConstructor
public class Transaction {

    private Long id;
    private LocalDate date;
    private BigDecimal amount;
    private TransactionScope scope;
    private String description;
    private double rate;
    private Long loanId;


}
