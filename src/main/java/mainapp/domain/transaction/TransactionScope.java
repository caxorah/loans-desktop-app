package mainapp.domain.transaction;

public enum TransactionScope {

    PRINCIPAL, INTERESTS
}
