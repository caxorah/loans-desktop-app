package mainapp.domain.transaction;

import java.util.List;

public interface TransactionRepository {


    List<Transaction> findAllByLoanId(Long loanId);

    Transaction save(Transaction transaction);

    void deleteById(Long id);

    void deleteAllByLoanId(Long loanId);
}
