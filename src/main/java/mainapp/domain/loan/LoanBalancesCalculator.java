package mainapp.domain.loan;

import mainapp.domain.interests.InterestCalculationItem;
import mainapp.domain.interests.InterestsCalculationPolicy;
import mainapp.domain.transaction.Transaction;
import mainapp.domain.transaction.TransactionScope;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class LoanBalancesCalculator {


    public static BigDecimal calculatePrincipalBalance(Loan loan, LocalDate asOfDate){
        return TransactionCalculator.summarizeTransactionsByScopeAndDate(loan.getTransactions(),asOfDate, TransactionScope.PRINCIPAL);
    }

    public static BigDecimal calculateInterestsBalance(Loan loan, LocalDate asOfDate){
        BigDecimal interestsCalculated = interestsCalculatedToDate(loan, asOfDate);
        BigDecimal interestsRepaid = TransactionCalculator.summarizeTransactionsByScopeAndDate(loan.getTransactions(),asOfDate, TransactionScope.INTERESTS);
        return interestsCalculated.add(interestsRepaid).setScale(2, RoundingMode.HALF_UP);
    }

    public static BigDecimal calculateInterestsForPeriod(Loan loan, LocalDate fromDate, LocalDate toDate) {
        return interestsCalculatedToDate(loan,toDate).subtract(interestsCalculatedToDate(loan, fromDate.minusDays(1)));
    }

    public static BigDecimal interestsCalculatedToDate(Loan loan, LocalDate asOfDate) {
        List<Transaction> transactionsList = buildTransactionsListForCalculationPurpose(loan, asOfDate);
        List<InterestCalculationItem> interestCalculationItems = mapTransactionsToInterestsCalculationItems(loan, transactionsList);
        return interestCalculationItems.stream()
                .map(InterestCalculationItem::calculateInterests)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    //    supporting methods

    private static List<Transaction> buildTransactionsListForCalculationPurpose(Loan loan, LocalDate toDate) {
        List<Transaction> transactions = loan.getTransactions().stream()
                .filter(t -> t.getDate().isBefore(toDate.plusDays(1)))
                .sorted(Comparator.comparing(Transaction::getDate))
                .collect(Collectors.toList());
        if (transactions.isEmpty()) return new ArrayList<>();

        Transaction lastTransaction = transactions.stream().max(Comparator.comparing(Transaction::getDate)).get();
        if (lastTransaction.getDate().isBefore(toDate)) {
            transactions.add(buildFakeEndingTransaction(toDate, lastTransaction.getRate()));
        }
        return transactions;
    }

    private static List<InterestCalculationItem> mapTransactionsToInterestsCalculationItems(Loan loan, List<Transaction> transactions) {
        List<InterestCalculationItem> interestCalculationItemList = new ArrayList<>();
        for (int i = 1; i< transactions.size(); i++){
            interestCalculationItemList.add(interestCalculationItemOf(
                            transactions.get(i-1),
                            transactions.get(i),
                            calculatePrincipalBalance(loan, transactions.get(i-1).getDate()),
                            loan.getInterestsCalculationPolicy()
                    ));
        }
        return interestCalculationItemList;
    }

    private static InterestCalculationItem interestCalculationItemOf(Transaction transaction1, Transaction transaction2, BigDecimal baseAmount, InterestsCalculationPolicy policy) {
        double rate = transaction1.getRate();
        return new InterestCalculationItem(transaction1.getDate(),transaction2.getDate(),rate,baseAmount,policy);
    }

    private static Transaction buildFakeEndingTransaction(LocalDate transactionDate, double transactionRate) {
        return new Transaction(
                null,
                transactionDate,
                BigDecimal.ZERO,
                TransactionScope.PRINCIPAL,
                "technical ending transaction",
                transactionRate,
                null);
    }


}
