package mainapp.domain.loan;

import mainapp.domain.transaction.Transaction;
import mainapp.domain.transaction.TransactionScope;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

public class TransactionCalculator {

    public static BigDecimal summarizeTransactionsByScopeAndDate(List<Transaction> transactionsList, LocalDate toDate, TransactionScope scope) {
        return transactionsList.stream()
                .filter(t -> t.getScope().equals(scope) && t.getDate().isBefore(toDate.plusDays(1)))
                .map(Transaction::getAmount)
                .reduce(BigDecimal.ZERO,BigDecimal::add);
    }
}
