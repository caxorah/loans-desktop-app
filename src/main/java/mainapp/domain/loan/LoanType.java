package mainapp.domain.loan;

public enum LoanType {

    LONG_TERM, SHORT_TERM, REVOLVING

}
