package mainapp.domain.loan;

public enum RepaymentStatus {

    NOT_STARTED,
    OPEN,
    PRINCIPAL_REPAID,
    PAID_OFF
}
