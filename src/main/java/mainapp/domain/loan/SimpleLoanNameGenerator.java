package mainapp.domain.loan;

import java.time.format.DateTimeFormatter;

public class SimpleLoanNameGenerator implements LoanNameGenerator {

    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyyMM");

    @Override
    public String getNameOf(Loan loan) {
        return loan.getLender().getEntityNumber() + "_" +
                loan.getBorrower().getEntityNumber() + "_" +
                loan.getLoanAmount().toPlainString() +
                loan.getCurrency().getCurrencyCode() + "_" +
                loan.getStartDate().format(DATE_FORMATTER);
    }
}
