package mainapp.domain.loan;

public interface LoanNameGenerator {

    String getNameOf(Loan loan);

}
