package mainapp.domain.loan;

import lombok.Builder;
import lombok.Value;
import mainapp.domain.company.Company;
import mainapp.domain.interests.InterestsCalculationPolicy;

import java.time.LocalDate;

@Builder
@Value
public class LoanRequest {

    Company lender;
    Company borrower;
    LocalDate startDate;
    LocalDate endDate;
    boolean autoExtend;
    String currency;
    InterestsCalculationPolicy interestsCalculationPolicy;
    double whtRate;
    double loanAmount;
    LoanType loanType;

}
