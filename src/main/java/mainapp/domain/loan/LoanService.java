package mainapp.domain.loan;

import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
public class LoanService {

    private final LoanRepository loanRepository;

    public List<Loan> getSummaryList() {
        return loanRepository.getAllLoans();
    }

    public Optional<Loan> getLoanDetails(Loan loan) {
        return loanRepository.getById(loan.getId());
    }

    public Loan save(Loan loan) {
        return loanRepository.save(loan);
    }

    public void delete(Loan loan) {
        loanRepository.delete(loan);
    }


}
