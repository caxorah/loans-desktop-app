package mainapp.domain.loan;

import java.math.BigDecimal;
import java.time.LocalDate;

class LoanStateCalculator {


    public static boolean checkIsExpired(Loan loan, LocalDate asOfDate) {
        if (loan.isAutoExtend() || loan.getEndDate()==null) {
            return false;
        }
        else {
            return loan.getEndDate().isBefore(asOfDate);
        }
    }

    public static RepaymentStatus checkRepaymentStatus(Loan loan, LocalDate asOfDate) {
        if (loan.getPrincipalBalance(asOfDate).compareTo(BigDecimal.ZERO)>0){
            return RepaymentStatus.OPEN;
        } else {
            if (loan.getInterestsBalance(asOfDate).compareTo(BigDecimal.ZERO)>0) {
                return RepaymentStatus.PRINCIPAL_REPAID;
            }
            return RepaymentStatus.PAID_OFF;
        }
    }










}
