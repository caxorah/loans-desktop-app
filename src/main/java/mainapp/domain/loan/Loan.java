package mainapp.domain.loan;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import mainapp.domain.company.Company;
import mainapp.domain.interests.InterestsCalculationPolicy;
import mainapp.domain.transaction.Transaction;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Currency;
import java.util.List;

@Builder
@Getter
@Setter
@ToString
public class Loan {

    private final LoanNameGenerator nameGenerator = new SimpleLoanNameGenerator();

    private Long id;
    private String name;
    private Company lender;
    private Company borrower;
    private LocalDate startDate;
    private LocalDate endDate;
    private boolean autoExtend;
    private Currency currency;
    private InterestsCalculationPolicy interestsCalculationPolicy;
    private BigDecimal loanAmount;
    private List<Transaction> transactions;
    private LoanType loanType;
    private String comment;

    public BigDecimal getPrincipalBalance(LocalDate asOfDate) {
        return LoanBalancesCalculator.calculatePrincipalBalance(this, asOfDate);
    }

    public BigDecimal getInterestsBalance(LocalDate asOfDate) {
        return LoanBalancesCalculator.calculateInterestsBalance(this, asOfDate);
    }

    public BigDecimal getInterestsForPeriod(LocalDate startDate, LocalDate endDate) {
        return LoanBalancesCalculator.calculateInterestsForPeriod(this, startDate, endDate);
    }

    public boolean isExpired(LocalDate asOfDate) {
        return LoanStateCalculator.checkIsExpired(this, asOfDate);
    }

    public RepaymentStatus getRepaymentStatus(LocalDate asOfDate) {
        return LoanStateCalculator.checkRepaymentStatus(this, asOfDate);
    }


}
