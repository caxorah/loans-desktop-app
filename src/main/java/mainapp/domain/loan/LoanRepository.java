package mainapp.domain.loan;

import java.util.List;
import java.util.Optional;

public interface LoanRepository {

    Loan save(Loan loan);

    List<Loan> getAllLoans();

    Optional<Loan> getById(Long id);

    void delete (Loan loan);

}
