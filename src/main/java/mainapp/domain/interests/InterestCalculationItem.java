package mainapp.domain.interests;

import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

@ToString
@Setter
public class InterestCalculationItem {

    private static final int FIXED_MONTH_LENGTH = 30;
    private static final int FIXED_YEAR_LENGTH = 360;
    private static final int STANDARD_YEAR_LENGTH = 365;

    private LocalDate fromDate;
    private LocalDate toDate;
    private BigDecimal numberOfDays;
    private InterestsCalculationPolicy interestsCalculationPolicy;
    private BigDecimal rate;
    private BigDecimal baseAmount;

    public InterestCalculationItem(LocalDate fromDate, LocalDate toDate, double rate, BigDecimal baseAmount, InterestsCalculationPolicy policy) {
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.interestsCalculationPolicy = policy;
        this.numberOfDays = BigDecimal.valueOf(getDays());
        this.rate = BigDecimal.valueOf(rate);
        this.baseAmount = baseAmount;
    }

    public BigDecimal calculateInterests(){
        switch (interestsCalculationPolicy) {
            case Y365DAYS:
                return calculateInterests(STANDARD_YEAR_LENGTH);
            case Y360DAYS:
                return calculateInterests(FIXED_YEAR_LENGTH);
            default:
                return BigDecimal.ZERO;
        }
    }

    private BigDecimal calculateInterests(int yearLength) {
        return baseAmount
                .multiply(rate)
                .multiply(numberOfDays)
                .divide(BigDecimal.valueOf(yearLength),2, RoundingMode.HALF_UP);
    }


    private long getDays() {
        switch (this.interestsCalculationPolicy) {
            case Y365DAYS:
                return days365();
            case Y360DAYS:
                return days360();
            default:
                return 0;
        }
    }

    private long days360() {
        long fullMonths = fromDate.until(toDate.plusDays(1), ChronoUnit.MONTHS);
        long additionalDaysBeginning = fromDate.until(fromDate.withDayOfMonth(fromDate.lengthOfMonth()), ChronoUnit.DAYS);
        long additionalDaysEnd = toDate.until(toDate.withDayOfMonth(toDate.lengthOfMonth()), ChronoUnit.DAYS);
        return fullMonths * FIXED_MONTH_LENGTH + additionalDaysBeginning + additionalDaysEnd;

    }

    private long days365() {
        return fromDate.until(toDate, ChronoUnit.DAYS);
    }


}
