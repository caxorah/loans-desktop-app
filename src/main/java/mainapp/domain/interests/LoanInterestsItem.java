package mainapp.domain.interests;

import lombok.Getter;
import mainapp.domain.PeriodConverter;
import mainapp.domain.loan.Loan;

import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
public class LoanInterestsItem {

    private final Loan loan;
    private final LocalDate interestPeriodStart;
    private final LocalDate interestPeriodEnd;
    private final BigDecimal interestsCalculatedForPeriod;
    private final String description;

    public LoanInterestsItem(Loan loan, LocalDate interestPeriodStart, LocalDate interestPeriodEnd, PeriodConverter periodConverter) {
        this.loan = loan;
        this.interestPeriodStart = interestPeriodStart;
        this.interestPeriodEnd = interestPeriodEnd;
        this.interestsCalculatedForPeriod = loan.getInterestsForPeriod(interestPeriodStart,interestPeriodEnd);
        this.description = buildDescription(periodConverter);
    }

    private String buildDescription(PeriodConverter periodConverter) {
        return periodConverter.convertDateToQ(interestPeriodEnd) + "Loan Interest payable to " + loan.getLender();
    }


}
