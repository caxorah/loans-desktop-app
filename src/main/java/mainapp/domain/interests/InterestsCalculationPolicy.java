package mainapp.domain.interests;

public enum InterestsCalculationPolicy {

    Y360DAYS,
    Y365DAYS
}
