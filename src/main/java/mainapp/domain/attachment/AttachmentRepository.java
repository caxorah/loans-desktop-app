package mainapp.domain.attachment;

import java.util.List;

public interface AttachmentRepository {

    Attachment save(Attachment attachment);

    List<Attachment> findByParentId(Long parentObjectId);

    void delete(Attachment attachment);
}
