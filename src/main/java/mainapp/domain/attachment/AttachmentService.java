package mainapp.domain.attachment;

import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

@Service
public class AttachmentService {

    private final AttachmentRepository attachmentRepository;

    public AttachmentService(AttachmentRepository attachmentRepository) {
        this.attachmentRepository = attachmentRepository;
    }

    public void save(Attachment attachment) {
        attachmentRepository.save(attachment);
    }

    public void saveFileToLoan(String filePath, String description, Long parentLoanId) {

        File file = new File(filePath);
        byte[] fileInBytes = new byte[(int) file.length()];
        try (FileInputStream fileInputStream = new FileInputStream(file)) {
            fileInputStream.read(fileInBytes);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Attachment att = new Attachment();
        att.setFileName(removeSpacesFromFileName(file.getName()));
        att.setParentId(parentLoanId);
        att.setDescription(description);
        att.setFile(fileInBytes);
        save(att);
    }

    public List<Attachment> getLoanAttachments(Long parentObjectId) {
        return attachmentRepository.findByParentId(parentObjectId);
    }

    public void openSelectedFile(Attachment attachment) {
        String outputFileLocation = defineDestinationPath(attachment.getFileName());
        File outputFile = new File(outputFileLocation);
        try (FileOutputStream fileOutputStream = new FileOutputStream(outputFile)){
            fileOutputStream.write(attachment.getFile());
            openFile(outputFileLocation);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void delete(Attachment selectedAttachment) {
        attachmentRepository.delete(selectedAttachment);
    }

    private static void openFile(String path) throws IOException {
        Runtime r= Runtime.getRuntime();
        r.exec("cmd /c " + path);
    }

    private static String defineDestinationPath(String filename){
        String userSpecific = System.getProperty("user.name");
        return  "C:\\Users\\" + userSpecific + "\\Downloads\\" + filename;
    }

    private static String removeSpacesFromFileName(String fileName){
        return fileName.replace(" ","_");
    }

}
