package mainapp.domain.attachment;

import lombok.Data;

@Data
public class Attachment {

    private Integer id;
    private byte[] file;
    private String fileName;
    private String description;
    private Long parentId;

}
