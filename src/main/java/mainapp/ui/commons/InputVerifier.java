package mainapp.ui.commons;

import javafx.scene.control.*;
import org.springframework.stereotype.Service;

@Service
public class InputVerifier {

    private final static String ERROR_STYLE = "-fx-text-fill: red; -fx-border-width: 1; -fx-border-color: red";
    private final static String OK_STYLE = "-fx-text-fill: black; -fx-border-width: 0";


    public boolean inputValueIsCorrect (Control control, VerifierParserInterface verifierParserInterface) {
        try {
            verifierParserInterface.verificationAction();
            control.setStyle(OK_STYLE);
            return true;
        } catch (Throwable t) {
            control.setStyle(ERROR_STYLE);
            return false;
        }
    }

    public boolean inputValueIsNotNull (Control control) {
        boolean result = false;
        if (control instanceof TextField) {
            result = ((TextField) control).getText()!=null && !((TextField) control).getText().isEmpty();
        }
        if (control instanceof ComboBox) {
            result = ((ComboBox<?>) control).getValue()!=null;
        }
        if (control instanceof DatePicker) {
            result = ((DatePicker) control).getValue()!=null;
        }

        if (result) {
            control.setStyle(OK_STYLE);
        } else {
            control.setStyle(ERROR_STYLE);
        }
        return result;
    }

    public boolean radioGroupHasOneSelected(ToggleGroup group){
        boolean result;
        String style;
        if (group.getSelectedToggle()==null) {
            result = false;
            style = ERROR_STYLE;
        } else {
            result = true;
            style = OK_STYLE;
        }
        group.getToggles().forEach(
                t -> ((RadioButton)t).setStyle(style)
        );
        return result;
    }



}
