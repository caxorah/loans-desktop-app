package mainapp.ui.commons;

import javafx.scene.layout.Pane;

import java.util.List;

public interface MyUIController<U,P extends Pane> {

    P getMainPane();
    void refreshItemsList(List<U> list);

}
