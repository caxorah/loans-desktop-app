package mainapp.ui.commons;

import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;


@Component
@Aspect
public class PopAlertMethodExecutor {

    @AfterThrowing(pointcut = "@annotation(popAlert)", throwing = "exc")
    public void execute(Exception exc, PopAlert popAlert) {
        javafx.scene.control.Alert alertWindow;
        if (exc instanceof InterruptedException) {
            alertWindow = AlertProducer.dBConnectionLostAlert(exc);
        }
        else {
            alertWindow = AlertProducer.unspecifiedErrorAlert(exc);
        }
        alertWindow.showAndWait();
    }

}
