package mainapp.ui.commons;

import javafx.beans.property.ObjectProperty;
import javafx.scene.Node;
import javafx.scene.layout.AnchorPane;
import org.springframework.stereotype.Component;

@Component
public class ViewLoader {

    private final SpringFXMLLoader springFXMLLoader;

    public ViewLoader(SpringFXMLLoader springFXMLLoader) {
        this.springFXMLLoader = springFXMLLoader;
    }

    public void loadView(String viewName, ObjectProperty<Node> whereToLoadView){
        String viewPath = "/fxml/"+viewName+".fxml";
        //this is a weak point, because now all my views must be based on Anchor Pane
        AnchorPane contentPane = (AnchorPane) springFXMLLoader.load(viewPath);
        whereToLoadView.set(contentPane);
    }


}
