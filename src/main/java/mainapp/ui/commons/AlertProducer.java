package mainapp.ui.commons;

import javafx.application.Preloader;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextArea;

import java.util.Arrays;

public class AlertProducer {

    //Exception alerts

    public static Alert dBConnectionLostAlert(Throwable exception){
        return basicErrorAlert("Database critical error",
                "Connection lost!" ,
                Arrays.toString(exception.getStackTrace()));
    }

    public static Alert unspecifiedErrorAlert(Throwable exception) {
        return basicErrorAlert("Unknown exception",
                "Unexpected error occured.",
                Arrays.toString(exception.getStackTrace()));
    }

    public static Alert applicationStartupErrorAlert (Preloader.ErrorNotification errorNotification) {
        return basicErrorAlert("Startup error",
                "Application could not be started. Database connection error.",
                errorNotification.getDetails());
    }

    //Information alerts

    public static Alert deleteConfirmationAlert(String nameOfObjectRemoved, String identifier) {
        Alert alert = new Alert(Alert.AlertType.NONE,
                "Are you sure you want to remove " + nameOfObjectRemoved + identifier + "?"
                ,ButtonType.YES,ButtonType.NO);
        alert.setTitle("Delete confirmation");
        return alert;
    }

    public static Alert batchNotFoundInfo() {
        return new Alert(Alert.AlertType.NONE, "Batch not found.", ButtonType.OK);
    }


    //basic error alert constructor

    private static Alert basicErrorAlert(String title, String header, String exceptionDetails) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle(title);
        alert.setHeaderText(header);

        TextArea area = new TextArea(exceptionDetails);
        area.setWrapText(true);
        area.setEditable(false);

        alert.getDialogPane().setExpandableContent(area);
        alert.setResizable(true);

        return alert;
    }

}
