package mainapp.ui.commons;

import java.time.LocalDate;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class PeriodService {

    public static int convertDateToPeriodInt(LocalDate date){
        return date.getYear()*100 + date.getMonth().getValue();
    }

    public static List<String> buildPeriodsListAllString(int minPeriod, int maxPeriod){
        YearMonth startPeriod = convertIntToYM(minPeriod).minusMonths(3);
        YearMonth endPeriod = convertIntToYM(maxPeriod).plusMonths(12);
        List<String> list = new ArrayList<>();
        for (YearMonth i=startPeriod; i.isBefore(endPeriod.plusMonths(1)); i=i.plusMonths(1) ){
            list.add(parseYMtoString(i));
        }
        return list;
    }

    public static String parseIntPeriodToString(int period) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMM");
        YearMonth ym = YearMonth.parse(String.valueOf(period),formatter);
        return parseYMtoString(ym);
    }

    public static String parseYMtoString(YearMonth ym){
        return ym.getMonth().name().substring(0,3) + "-" + (ym.getYear()-2000);
    }

    public static int parseStringPeriodToInt(String periodString){
        String properCasePeriodName = periodString.substring(0,1).toUpperCase() + periodString.substring(1).toLowerCase();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMM-yy", Locale.US);
        YearMonth ym = YearMonth.from(formatter.parse(properCasePeriodName));
        return ym.getYear()*100 + ym.getMonthValue();
    }

    public static YearMonth convertIntToYM(int periodInt){
        int yr = periodInt/100;
        int mon = periodInt - yr*100;
        return YearMonth.of(yr,mon);
    }



}
