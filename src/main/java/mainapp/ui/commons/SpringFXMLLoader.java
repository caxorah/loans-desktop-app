package mainapp.ui.commons;

import javafx.fxml.FXMLLoader;
import lombok.SneakyThrows;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class SpringFXMLLoader implements ApplicationContextAware {

    private ApplicationContext ctx;

    public SpringFXMLLoader(ApplicationContext ctx) {
        this.ctx = ctx;
    }

    @SneakyThrows
    public Object load(String url) {
        ConfigurableApplicationContext configCtx = ((ConfigurableApplicationContext) ctx);
        FXMLLoader loader = new FXMLLoader(getClass().getResource(url));
        loader.setControllerFactory(configCtx::getBean);
        return loader.load();
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) {
        this.ctx=applicationContext;
    }
}
