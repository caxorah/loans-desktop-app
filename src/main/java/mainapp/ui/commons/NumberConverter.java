package mainapp.ui.commons;

import javafx.scene.control.TextField;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.text.DecimalFormat;

@Component
public class NumberConverter {

    public static String valueToString(Number o) {
        return o==null ? null : o.toString();
    }

    public Integer parseTextInputToInteger(TextField textField){
        String textInput = textField.getText();
        try {
            if (textInput==null || textInput.isEmpty()) {
                return null;
            } else {
                return Integer.valueOf(textInput);
            }
        } catch (NumberFormatException e) {
            setInputTextRed(textField);
            throw new NumberFormatException();
        }
    }

    public Double parseTextInputToDouble(TextField textField){
        String textInput = textField.getText();
        try {
            if (textInput==null || textInput.isEmpty()) {
                return null;
            } else {
                return Double.valueOf(textInput);
            }
        } catch (NumberFormatException e) {
            setInputTextRed(textField);
            throw new NumberFormatException();
        }
    }

    public Long parseTextInputToLong(TextField textField) {
        String textInput = textField.getText();
        try {
            if (textInput==null || textInput.isEmpty()) {
                return null;
            } else {
                return Long.valueOf(textInput);
            }
        } catch (NumberFormatException e) {
            setInputTextRed(textField);
            throw new NumberFormatException();
        }
    }

    public BigDecimal parseTextInputToBigDecimal(TextField textField) {
        String textInput = textField.getText();
        try {
            if (textInput==null || textInput.isEmpty()) {
                return null;
            } else {
                return new BigDecimal(textInput);
            }
        } catch (Exception e) {
            setInputTextRed(textField);
            throw new NumberFormatException();
        }
    }

    public void setInputTextRed (TextField textField) {
        textField.setStyle("-fx-text-fill: red");
    }

    public static String parseBigDecimalToFormattedString(BigDecimal bigDecimal) {
        return new DecimalFormat("#,##0.00").format(bigDecimal);
    }

    public static String parseDoubleToFormattedPercentage(Double value) {
        return new DecimalFormat("#0.0000").format(value*100);
    }

    public static double convertTextToRate(String rateAsText) {
        double inputAsDouble = Double.parseDouble(rateAsText);
        return inputAsDouble/100;
    }
}
