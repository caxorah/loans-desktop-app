package mainapp.ui.commons;

import javafx.concurrent.Task;
import javafx.scene.Cursor;

import java.util.List;

public class TaskFactory<T extends MyServices<U>, U, V extends MyUIController<U,?>> {

    T serviceBean;
    V viewController;

    public TaskFactory(T serviceBean , V viewController) {
        this.serviceBean = serviceBean;
        this.viewController = viewController;
    }

    private Task<Void> buildSaveTask(U modelObject) {
        Task<Void> saveTask = new Task<Void>() {
            @Override
            protected Void call() {
                serviceBean.save(modelObject);
                return null;
            }
        };
        setUpCursorChanges(saveTask);
        saveTask.setOnSucceeded((evt) -> runInDaemonThread(this.buildGetAllTask()));
        return saveTask;
    }

    private Task<Void> buildDeleteTask(U modelObject) {
        Task<Void> deleteTask = new Task<Void>() {
            @Override
            protected Void call() {
                serviceBean.delete(modelObject);
                return null;
            }
        };
        setUpCursorChanges(deleteTask);
        deleteTask.setOnSucceeded((evt) -> runInDaemonThread(this.buildGetAllTask()));
        return deleteTask;
    }

    private Task<List<U>> buildGetAllTask() {
        Task<List<U>> getAllTask = new Task<List<U>>() {
            @Override
            protected List<U> call() {
                return serviceBean.getAll();
            }
        };
        setUpCursorChanges(getAllTask);
        getAllTask.setOnSucceeded((evt) -> {
            viewController.refreshItemsList(getAllTask.getValue());
            viewController.getMainPane().setCursor(Cursor.DEFAULT);
        });
        return getAllTask;
    }




    public void runSaveTask(U modelObject) {
        runInDaemonThread(buildSaveTask(modelObject));
    }

    public void runDeleteTask(U modelObject) {
        runInDaemonThread(buildDeleteTask(modelObject));
    }

    public void runGetAllTask() {
        runInDaemonThread(buildGetAllTask());
    }

    public static void runInDaemonThread(Task<?> task) {
        Thread thread = new Thread(task);
        thread.setDaemon(true);
        thread.start();
    }

    private void setUpCursorChanges(Task<?> saveTask) {
        saveTask.setOnRunning((evt) -> viewController.getMainPane().setCursor(Cursor.WAIT));
        saveTask.setOnFailed((evt) -> viewController.getMainPane().setCursor(Cursor.DEFAULT));
    }

}
