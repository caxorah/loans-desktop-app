package mainapp.ui.commons;

import java.util.List;

public interface MyServices<T> {

    void save(T objectToSave);
    void delete(T objectToDelete);
    List<T> getAll();

}
