package mainapp.ui.company;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import lombok.RequiredArgsConstructor;
import mainapp.domain.company.Company;
import mainapp.ui.commons.MyUIController;
import mainapp.ui.commons.TaskFactory;
import org.springframework.stereotype.Component;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

@Component
@RequiredArgsConstructor
public class CompaniesViewController implements Initializable, MyUIController<Company, AnchorPane> {

    private final CompanyUIService service;
    private TaskFactory<CompanyUIService ,Company, CompaniesViewController> taskFactory;

    @FXML private AnchorPane mainPane;
    @FXML private TableView<Company> companiesTable;
    @FXML private TableColumn<Company, Integer> colEntity;
    @FXML private TableColumn<Company, String> colName;
    @FXML private TextField entityNumInput;
    @FXML private TextField nameInput;

    private Company selectedCompany;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        taskFactory = new TaskFactory<>(service,this);
        colEntity.setCellValueFactory(new PropertyValueFactory<>("entityNumber"));
        colName.setCellValueFactory(new PropertyValueFactory<>("name"));
        taskFactory.runGetAllTask();

        companiesTable.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (newSelection != null) setSelectedCompany(newSelection);
        });
        selectedCompany = new Company();
    }

    @FXML
    void deleteCompany() {
        //TODO implement checks or do not allow to delete at all
    }

    @FXML
    void saveCompany() {
        Company companyToSave = composeCompanyFromFieldsValues();
        if (companyToSave!=null) {
            taskFactory.runSaveTask(companyToSave);
        }
        clearInputs();
    }

    @FXML void clearInputs() {
        entityNumInput.setText(null);
        nameInput.setText(null);
        entityNumInput.setStyle(null);
    }

    private void setValuesToInputs() {
        entityNumInput.setText(String.valueOf(selectedCompany.getEntityNumber()));
        nameInput.setText(selectedCompany.getName());
        entityNumInput.setStyle(null);
    }

    private void setSelectedCompany(Company company) {
        this.selectedCompany = company;
        setValuesToInputs();
    }

    private Company composeCompanyFromFieldsValues() {
        Company company = null;
        try {
             company = new Company(
                    Integer.parseInt(entityNumInput.getText()),
                    nameInput.getText()
            );
        } catch (NumberFormatException e) {
            entityNumInput.setStyle("-fx-text-fill: red");
        }
        return company;
    }


    @Override
    public AnchorPane getMainPane() {
        return this.mainPane;
    }

    @Override
    public void refreshItemsList(List<Company> list) {
        ObservableList<Company> listOfTypes = FXCollections.observableList(list);
        companiesTable.setItems(listOfTypes);
    }
}
