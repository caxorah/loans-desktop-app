package mainapp.ui.company;

import mainapp.domain.company.Company;
import mainapp.domain.company.CompanyRepository;
import mainapp.domain.company.CompanyService;
import mainapp.ui.commons.MyServices;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class CompanyUIService extends CompanyService implements MyServices<Company> {

    public CompanyUIService(CompanyRepository companyRepository) {
        super(companyRepository);
    }

    @Override
    public void save(Company company) {
        super.save(company);
    }

    @Override
    public void delete(Company company) {
        //companies cannot be deleted to ensure data integrity
    }

    @Override
    public List<Company> getAll() {
        return super.getAll();
    }

    public Company getCompanyByNumber(int entityNumber) {
        return super.getCompanyByNumber(entityNumber);
    }

}
