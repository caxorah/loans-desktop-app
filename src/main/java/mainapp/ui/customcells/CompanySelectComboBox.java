package mainapp.ui.customcells;

import javafx.scene.control.ListCell;
import mainapp.domain.company.Company;

public class CompanySelectComboBox extends ListCell<Company> {

    @Override
    protected void updateItem(Company item, boolean empty) {
        super.updateItem(item, empty);
        setText(empty ? "" : String.valueOf(item.getEntityNumber()));
    }
}
