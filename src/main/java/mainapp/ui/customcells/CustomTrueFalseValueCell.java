package mainapp.ui.customcells;

import javafx.scene.control.TableCell;

public class CustomTrueFalseValueCell<S> extends TableCell<S,Boolean> {

    private final String valueForTrue;
    private final String valueForFalse;

    public CustomTrueFalseValueCell(String valueForTrue, String valueForFalse) {
        this.valueForTrue = valueForTrue;
        this.valueForFalse = valueForFalse;
    }

    @Override
    protected void updateItem(Boolean aBoolean, boolean b) {
        this.setText( !b && aBoolean ? valueForTrue : valueForFalse );
    }

}
