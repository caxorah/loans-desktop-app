package mainapp.ui.customcells;

import javafx.scene.control.TableCell;
import javafx.scene.text.Text;

public class WrappingTextTableCell<S> extends TableCell<S,String> {

    private final Text cellText;

    public WrappingTextTableCell() {
        super();
        this.cellText = createText();
    }

    @Override
    public void updateItem(String item, boolean empty) {
        super.updateItem(item, empty);
        if (!isEmpty()) {
            setGraphic(cellText);
        }
    }

    private Text createText() {
        Text text = new Text();
        text.wrappingWidthProperty().bind(widthProperty());
        text.textProperty().bind(itemProperty());
        return text;
    }

}
