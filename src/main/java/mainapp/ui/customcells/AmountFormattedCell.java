package mainapp.ui.customcells;

import javafx.scene.control.TableCell;
import mainapp.ui.commons.NumberConverter;

import java.math.BigDecimal;

public class AmountFormattedCell<S> extends TableCell<S, BigDecimal> {

    @Override
    protected void updateItem(BigDecimal item, boolean empty) {
        this.setText(empty || item == null ? "" : NumberConverter.parseBigDecimalToFormattedString(item));
    }
}
