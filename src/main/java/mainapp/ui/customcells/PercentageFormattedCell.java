package mainapp.ui.customcells;

import javafx.scene.control.TableCell;
import mainapp.ui.commons.NumberConverter;

public class PercentageFormattedCell<S> extends TableCell<S, Double> {

    @Override
    protected void updateItem(Double item, boolean empty) {
        this.setText(empty || item == null ? "" : NumberConverter.parseDoubleToFormattedPercentage(item) + "%");
    }
}
