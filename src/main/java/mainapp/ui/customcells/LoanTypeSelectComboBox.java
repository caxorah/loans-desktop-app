package mainapp.ui.customcells;

import javafx.scene.control.ListCell;
import mainapp.domain.loan.LoanType;

public class LoanTypeSelectComboBox extends ListCell<LoanType> {

    @Override
    protected void updateItem(LoanType item, boolean empty) {
        super.updateItem(item, empty);
        setText(empty ? "" : String.valueOf(item));
    }
}
