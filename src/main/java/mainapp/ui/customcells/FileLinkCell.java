package mainapp.ui.customcells;

import javafx.scene.control.Hyperlink;
import javafx.scene.control.TableCell;
import mainapp.domain.attachment.Attachment;
import mainapp.domain.attachment.AttachmentService;


public class FileLinkCell extends TableCell<Attachment,String> {

    private final AttachmentService attachmentService;

    public FileLinkCell(AttachmentService attachmentService) {
        this.attachmentService = attachmentService;
    }

    @Override
    protected void updateItem(String link, boolean empty) {
        super.updateItem(link, empty);
        setText(null);
        if (!empty) {
            Hyperlink hyperlink = new Hyperlink(link);
            Attachment selectedAttachment = getTableRow().getItem();
            hyperlink.setOnAction(a-> attachmentService.openSelectedFile(selectedAttachment));
            setGraphic(hyperlink);
            hyperlink.setStyle("-fx-text-fill: #0c1649");
        } else {
            setGraphic(null);
        }
    }

}
