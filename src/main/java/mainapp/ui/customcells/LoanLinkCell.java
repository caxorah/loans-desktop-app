package mainapp.ui.customcells;

import javafx.scene.control.Hyperlink;
import javafx.scene.control.TableCell;
import mainapp.ui.loans.LoanDto;
import mainapp.ui.loans.LoansViewController;

public class LoanLinkCell extends TableCell<LoanDto, String> {

    private final LoansViewController loansViewController;

    public LoanLinkCell(LoansViewController loansViewController) {
        this.loansViewController = loansViewController;
    }

    @Override
    protected void updateItem(String loanName, boolean empty) {
        super.updateItem(loanName, empty);
        setText(null);
        if (!empty) {
            Hyperlink hyperlink = new Hyperlink(loanName);
            LoanDto selectedLoan = getTableRow().getItem();
            hyperlink.setOnAction(l -> loansViewController.loadLoanDetailsForm(selectedLoan));
            setGraphic(hyperlink);
            hyperlink.setStyle("-fx-text-fill: #0c1649");
        } else {
            setGraphic(null);
        }
    }
}
