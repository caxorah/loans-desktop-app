package mainapp.ui.home;

import javafx.fxml.FXML;
import javafx.scene.layout.BorderPane;
import mainapp.ui.commons.ViewLoader;
import org.springframework.stereotype.Component;


@Component
public class HomeController {

    private final ViewLoader viewLoader;
    @FXML private BorderPane mainPane;

    public HomeController(ViewLoader viewLoader) {
        this.viewLoader = viewLoader;
    }

    @FXML
    void loadLoansView() {
        loadViewToCenterPart("loansView");
    }

    @FXML
    void loadCompaniesView() {
        loadViewToCenterPart("companiesView");
    }

    @FXML
    void loadBlankLoanForm() {
        loadViewToCenterPart("loanForm");
    }

    @FXML
    void loadInterestsView() {
        loadViewToCenterPart("interestsView");
    }

    public void loadViewToCenterPart(String viewName){
        viewLoader.loadView(viewName, mainPane.centerProperty());
    }

}
