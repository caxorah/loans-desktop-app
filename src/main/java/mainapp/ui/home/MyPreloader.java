package mainapp.ui.home;

import javafx.application.Preloader;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import mainapp.ui.commons.AlertProducer;

import java.util.Optional;

public class MyPreloader extends Preloader {

    private Stage preloaderStage;
    private Scene preloaderScene;

    @Override
    public void init() throws Exception {
        preloaderScene = new Scene(FXMLLoader.load(getClass().getResource("/fxml/loading.fxml")));
    }

    @Override
    public void start(Stage stage) {
        preloaderStage = stage;
        preloaderStage.setScene(preloaderScene);
        preloaderStage.initStyle(StageStyle.UNDECORATED);
        preloaderStage.show();
    }

    @Override
    public void handleStateChangeNotification(Preloader.StateChangeNotification stateChangeNotification) {
        StateChangeNotification.Type type = stateChangeNotification.getType();
        if (type == StateChangeNotification.Type.BEFORE_START) {
            preloaderStage.hide();
        }
    }

    @Override
    public boolean handleErrorNotification(ErrorNotification errorNotification) {
        Alert alert = AlertProducer.applicationStartupErrorAlert(errorNotification);
        Optional<ButtonType> result = alert.showAndWait();
        if (result.isPresent() && result.get() == ButtonType.OK) {
            preloaderStage.hide();
        }
        return true;
    }

}
