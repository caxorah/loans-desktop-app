package mainapp.ui;

import com.sun.javafx.application.LauncherImpl;
import javafx.application.Application;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import mainapp.MainApp;
import mainapp.ui.home.MyPreloader;
import mainapp.ui.commons.SpringFXMLLoader;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ConfigurableApplicationContext;

public class FXUIApp extends Application {

    ConfigurableApplicationContext ctx;

    @Override
    public void init() {
        ctx = SpringApplication.run(MainApp.class);
    }

    @Override
    public void start(Stage primaryStage) {

        Parent root = (Parent) new SpringFXMLLoader(ctx).load("/fxml/home.fxml");
        primaryStage.setScene(new Scene(root));
        primaryStage.setTitle("ICO Manager");
        primaryStage.setMaximized(true);
        primaryStage.show();

    }

    public static void main(String[] args) {
        LauncherImpl.launchApplication(FXUIApp.class, MyPreloader.class, args);
    }

}
