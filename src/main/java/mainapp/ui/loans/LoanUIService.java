package mainapp.ui.loans;

import mainapp.domain.loan.Loan;
import mainapp.domain.loan.LoanRepository;
import mainapp.domain.loan.LoanService;
import mainapp.ui.commons.MyServices;
import mainapp.ui.company.CompanyUIService;
import mainapp.ui.loans.details.TransactionUIService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class LoanUIService extends LoanService implements MyServices<LoanDto> {

    private final CompanyUIService companyUIService;
    private final TransactionUIService transactionUIService;

    public LoanUIService(LoanRepository loanRepository, CompanyUIService companyUIService, TransactionUIService transactionUIService) {
        super(loanRepository);
        this.companyUIService = companyUIService;
        this.transactionUIService = transactionUIService;
    }

    @Override
    public List<LoanDto> getAll() {
        LocalDate stateDate = LocalDate.now();
        List<Loan> loansSummary = super.getSummaryList();
        return loansSummary.stream().map(l -> new LoanDto(l,stateDate)).collect(Collectors.toList());
    }

    @Override
    public void save(LoanDto loanDto) {
        Loan loan = loanDto.toDomain(companyUIService);
        this.save(loan);
    }


    public LoanDto saveNew(LoanDto loanDto) {
        Loan newLoan = loanDto.toDomain(companyUIService);
        Loan saved = super.save(newLoan);
        return new LoanDto(saved,LocalDate.now());
    }

    @Override
    public void delete(LoanDto loanDto) {
        Loan loanToDelete = loanDto.toDomain(companyUIService);
        super.delete(loanToDelete);
        transactionUIService.setLoanId(loanToDelete.getId());
        transactionUIService.deleteLoanTransactions();
    }

    public LoanDto getLoan(LoanDto loanDto){
        LocalDate stateDate = loanDto.getStateDate();
        Optional<Loan> loan = getLoanDetails(loanDto.toDomain(companyUIService));
        return new LoanDto(loan.get(),stateDate);
    }



}
