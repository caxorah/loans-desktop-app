package mainapp.ui.loans;

import mainapp.domain.loan.LoanNameGenerator;
import mainapp.domain.loan.SimpleLoanNameGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class LoansConfig {

    @Bean
    public LoanNameGenerator loanNameGenerator() {
        return new SimpleLoanNameGenerator();
    }

}
