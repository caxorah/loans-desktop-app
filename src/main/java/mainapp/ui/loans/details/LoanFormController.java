package mainapp.ui.loans.details;

import javafx.collections.FXCollections;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.util.Callback;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import mainapp.domain.interests.InterestsCalculationPolicy;
import mainapp.domain.loan.LoanType;
import mainapp.ui.commons.InputVerifier;
import mainapp.ui.commons.NumberConverter;
import mainapp.ui.commons.TaskFactory;
import mainapp.ui.company.CompanyUIService;
import mainapp.ui.customcells.LoanTypeSelectComboBox;
import mainapp.ui.home.HomeController;
import mainapp.ui.loans.LoanDto;
import mainapp.ui.loans.LoanUIService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Component
public class LoanFormController implements Initializable {

    @FXML private AnchorPane mainPane;
    @FXML private ComboBox<Integer> lenderCombo;
    @FXML private Label lenderNameLabel;
    @FXML private ComboBox<Integer> borrowerCombo;
    @FXML private Label borrowerNameLabel;
    @FXML private Label loanNameLabel;
    @FXML private DatePicker startDateInput;
    @FXML private DatePicker endDateInput;
    @FXML private ComboBox<LoanType> loanTypeCombo;
    @FXML private CheckBox autoExtendCheckbox;
    @FXML private TextField amountInput;
    @FXML private TextField currencyInput;
    @FXML private TextArea commentInput;
    @FXML private ComboBox<InterestsCalculationPolicy> interestsCalcPolicyCombo;
    @FXML private Button cancelButton;
    @FXML private Button deleteButton;

    LoanDetailsContainerController loanDetailsContainerController;
    @Autowired @Lazy
    public void setLoanDetailsContainerController(LoanDetailsContainerController loanDetailsContainerController) {
        this.loanDetailsContainerController = loanDetailsContainerController;
    }

    private final CompanyUIService companyUIService;
    private final NumberConverter numberConverter;
    private final LoanUIService loanUIService;
    private final InputVerifier inputVerifier;
    private final HomeController homeController;

    private LoanDto selectedLoan;
    private final Map<Integer,String> allCompanyNames = new HashMap<>();
    private LoanDetailsTaskFactory taskFactory;

    @SneakyThrows
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.selectedLoan = new LoanDto();
        Thread thread = new Thread(()->companyUIService.getAll()
                .forEach(c -> allCompanyNames.put(c.getEntityNumber(), c.getName())));
        thread.start();
        thread.join();
        assignComboLists();
        updateFieldsValues();
        deleteButton.setVisible(false);
    }

    public void setLoan(LoanDto loanDto) {
        this.selectedLoan = loanDto;
        this.taskFactory = new LoanDetailsTaskFactory(loanDetailsContainerController);
        cancelButton.setVisible(selectedLoan.getId()==null);
        deleteButton.setVisible(selectedLoan.getId()!=null);
        updateFieldsValues();
    }

    @FXML void updateBorrowerName() {
        Integer entity = borrowerCombo.getValue();
        borrowerNameLabel.setText(entity==null ? "" : allCompanyNames.get(entity));
    }

    @FXML
    void updateLenderName() {
        Integer entity = lenderCombo.getValue();
        lenderNameLabel.setText(entity==null ? "" : allCompanyNames.get(entity));
    }

    @FXML
    void loadLoansView() {
        homeController.loadViewToCenterPart("loansView");
    }

    @FXML
    void saveLoan() {
        if (allInputValuesCorrect()) {
            updateLoanWithNewValues();
            if (selectedLoan.getId()==null) {
                saveNewLoan();
            }
            else {
                updateExistingLoan();
            }
        }
    }

    @FXML
    void deleteThisLoan() {
        LoanDto loanToDelete = selectedLoan;
        Task<Void> deleteTask = new Task<Void>() {
            @Override
            protected Void call() {
                loanUIService.delete(loanToDelete);
                return null;
            }};
        deleteTask.setOnRunning((e) -> mainPane.setCursor(Cursor.WAIT));
        deleteTask.setOnFailed((e) -> mainPane.setCursor(Cursor.DEFAULT));
        deleteTask.setOnSucceeded( (e) -> {
            homeController.loadViewToCenterPart("loansView");
            mainPane.setCursor(Cursor.DEFAULT);
        });
        TaskFactory.runInDaemonThread(deleteTask);
    }

    private void updateExistingLoan() {
        Task<Void> saveTask = new Task<Void>() {
            @Override
            protected Void call() {
                loanUIService.save(selectedLoan);
                return null;
        }};
        taskFactory.runTaskAndRecalculate(saveTask);
    }

    private void saveNewLoan() {
        Task<LoanDto> saveNewTask = new Task<LoanDto>() {
            @Override
            protected LoanDto call() {
                return loanUIService.saveNew(selectedLoan);
        }};
        saveNewTask.setOnRunning((e) -> mainPane.setCursor(Cursor.WAIT));
        saveNewTask.setOnFailed((e) -> mainPane.setCursor(Cursor.DEFAULT));
        saveNewTask.setOnSucceeded( (e) -> {
            homeController.loadViewToCenterPart("loanDetailsContainer");
            loanDetailsContainerController.setLoan(saveNewTask.getValue());
        });
        TaskFactory.runInDaemonThread(saveNewTask);
    }

    private void assignComboLists() {
        List<Integer> entities = allCompanyNames.keySet().stream().sorted(Integer::compareTo).collect(Collectors.toList());
        lenderCombo.setItems(FXCollections.observableArrayList(entities));
        borrowerCombo.setItems(FXCollections.observableArrayList(entities));
        List<LoanType> loanTypes = Arrays.asList(LoanType.values());
        loanTypeCombo.setItems(FXCollections.observableList(loanTypes));
        Callback<ListView<LoanType>, ListCell<LoanType>> listCellCallback = c -> new LoanTypeSelectComboBox();
        loanTypeCombo.setCellFactory(listCellCallback);
        loanTypeCombo.setButtonCell(listCellCallback.call(null));
        interestsCalcPolicyCombo.setItems(FXCollections.observableArrayList(InterestsCalculationPolicy.values()));
    }

    private void updateFieldsValues() {
        lenderCombo.setValue(selectedLoan.getLender());
        borrowerCombo.setValue(selectedLoan.getBorrower());
        updateLenderName();
        updateBorrowerName();
        loanNameLabel.setText(selectedLoan.getName());
        startDateInput.setValue(selectedLoan.getStartDate());
        endDateInput.setValue(selectedLoan.getEndDate());
        autoExtendCheckbox.setSelected(selectedLoan.isAutoExtend());
        amountInput.setText(selectedLoan.getInitialPrincipalAmount() == null ? "" : selectedLoan.getInitialPrincipalAmount().toString());
        currencyInput.setText(selectedLoan.getCurrency());
        interestsCalcPolicyCombo.setValue(selectedLoan.getInterestsCalculationPolicy());
        loanTypeCombo.setValue(selectedLoan.getLoanType());
        commentInput.setText(selectedLoan.getComment());
    }

    private void updateLoanWithNewValues() {
        LoanDto loanDtoFromValues = new LoanDto();
        loanDtoFromValues.setLender(lenderCombo.getValue());
        loanDtoFromValues.setBorrower(borrowerCombo.getValue());
        loanDtoFromValues.setStartDate(startDateInput.getValue());
        loanDtoFromValues.setEndDate(endDateInput.getValue());
        loanDtoFromValues.setAutoExtend(autoExtendCheckbox.isSelected());
        loanDtoFromValues.setInitialPrincipalAmount(numberConverter.parseTextInputToBigDecimal(amountInput));
        loanDtoFromValues.setCurrency(currencyInput.getText());
        loanDtoFromValues.setInterestsCalculationPolicy(interestsCalcPolicyCombo.getValue());
        loanDtoFromValues.setLoanType(loanTypeCombo.getValue());
        loanDtoFromValues.setComment(commentInput.getText());

        loanDtoFromValues.setName(selectedLoan.getName());
        loanDtoFromValues.setId(selectedLoan.getId());
        loanDtoFromValues.setTransactions(selectedLoan.getTransactions()==null ?
                new ArrayList<>() : selectedLoan.getTransactions());

        this.selectedLoan = loanDtoFromValues;
    }

    private boolean allInputValuesCorrect () {
        return inputVerifier.inputValueIsNotNull(lenderCombo) &&
                inputVerifier.inputValueIsNotNull(borrowerCombo) &&
                inputVerifier.inputValueIsNotNull(startDateInput) &&
                (endDateInput.getValue()==null || endDateInput.getValue().isAfter(startDateInput.getValue())) &&
                inputVerifier.inputValueIsNotNull(amountInput) &&
                inputVerifier.inputValueIsCorrect(amountInput,() -> numberConverter.parseTextInputToBigDecimal(amountInput)) &&
                inputVerifier.inputValueIsCorrect(currencyInput, () -> Currency.getInstance(currencyInput.getText())) &&
                inputVerifier.inputValueIsNotNull(interestsCalcPolicyCombo) &&
                inputVerifier.inputValueIsNotNull(loanTypeCombo);
    }

}
