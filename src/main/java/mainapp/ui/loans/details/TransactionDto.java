package mainapp.ui.loans.details;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import mainapp.domain.transaction.Transaction;
import mainapp.domain.transaction.TransactionScope;

import java.math.BigDecimal;
import java.time.LocalDate;

@ToString
@Setter
@Getter
@Builder
public class TransactionDto {

    private Long id;
    private LocalDate date;
    private BigDecimal principalChange;
    private BigDecimal interestsChange;
    private double rate;
    private String description;
    private Long loanId;

    public static TransactionDto of(Transaction transaction) {
        TransactionDto dto = TransactionDto.builder()
                .id(transaction.getId())
                .date(transaction.getDate())
                .rate(transaction.getRate())
                .description(transaction.getDescription())
                .loanId(transaction.getLoanId())
                .build();
        switch (transaction.getScope()) {
            case PRINCIPAL:
                dto.setPrincipalChange(transaction.getAmount());
                break;
            case INTERESTS:
                dto.setInterestsChange(transaction.getAmount());
                break;
        }
        return dto;
    }

    public Transaction toDomain() {
        Transaction domain = Transaction.builder()
                .id(this.id)
                .date(this.date)
                .description(this.description)
                .rate(this.rate)
                .loanId(this.loanId)
                .build();
        if (this.principalChange!=null) {
            domain.setScope(TransactionScope.PRINCIPAL);
            domain.setAmount(this.principalChange);
        } else {
            domain.setScope(TransactionScope.INTERESTS);
            domain.setAmount(this.interestsChange);
        }
        return domain;
    }

}
