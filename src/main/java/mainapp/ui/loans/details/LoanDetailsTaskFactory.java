package mainapp.ui.loans.details;

import javafx.concurrent.Task;
import javafx.scene.Cursor;
import mainapp.ui.commons.TaskFactory;

public class LoanDetailsTaskFactory {

    private final LoanDetailsContainerController loanDetailsContainerController;

    public LoanDetailsTaskFactory(LoanDetailsContainerController loanDetailsContainerController) {
        this.loanDetailsContainerController = loanDetailsContainerController;
    }

    public void runTaskAndRecalculate(Task<Void> task) {
        task.setOnSucceeded((e) -> {
            loanDetailsContainerController.refreshLoan();
            loanDetailsContainerController.getMainPane().setCursor(Cursor.DEFAULT);
        });
        task.setOnRunning((e)-> loanDetailsContainerController.getMainPane().setCursor(Cursor.WAIT));
        task.setOnFailed((e) -> loanDetailsContainerController.getMainPane().setCursor(Cursor.DEFAULT));
        TaskFactory.runInDaemonThread(task);
    }


}
