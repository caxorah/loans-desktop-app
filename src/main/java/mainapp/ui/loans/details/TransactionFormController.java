package mainapp.ui.loans.details;

import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import lombok.RequiredArgsConstructor;
import mainapp.domain.transaction.Transaction;
import mainapp.ui.commons.InputVerifier;
import mainapp.ui.commons.NumberConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.net.URL;
import java.util.ResourceBundle;

@Component
@RequiredArgsConstructor
public class TransactionFormController implements Initializable {

    private final TransactionUIService transactionUIService;
    private final InputVerifier inputVerifier;
    private final NumberConverter numberConverter;

    private LoanDetailsContainerController loanDetailsContainerController;
    @Autowired
    @Lazy
    public void setLoanDetailsContainerController(LoanDetailsContainerController loanDetailsContainerController) {
        this.loanDetailsContainerController = loanDetailsContainerController;
    }

    @FXML private TextField amountInput;
    @FXML private DatePicker dateInput;
    @FXML private TextField rateInput;
    @FXML private TextField descriptionInput;
    @FXML private RadioButton interestsRadio;
    @FXML private RadioButton principalRadio;
    @FXML private RadioButton rateRadio;

    private TransactionDto selectedTransaction;
    private LoanDetailsTaskFactory taskFactory;
    private ToggleGroup radioGroup;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        taskFactory = new LoanDetailsTaskFactory(loanDetailsContainerController);
        radioGroup = new ToggleGroup();
        principalRadio.setToggleGroup(radioGroup);
        interestsRadio.setToggleGroup(radioGroup);
        rateRadio.setToggleGroup(radioGroup);
        amountInput.visibleProperty().bind(rateRadio.selectedProperty().not());
    }

    public void setSelectedTransaction(TransactionDto selectedTransaction) {
        this.selectedTransaction = selectedTransaction;
        populateInputValues();
    }

    private void populateInputValues() {
        Transaction transaction = selectedTransaction.toDomain();
        dateInput.setValue(selectedTransaction.getDate());
        descriptionInput.setText(selectedTransaction.getDescription());
        rateInput.setText(String.valueOf(selectedTransaction.getRate()*100));
        amountInput.setText(transaction.getAmount()==null ? "" : transaction.getAmount().toString());

        switch (transaction.getScope()) {
            case PRINCIPAL:
                principalRadio.setSelected(true);
                break;
            case INTERESTS:
                if (transaction.getAmount()!=null){
                    if (transaction.getAmount().equals(BigDecimal.ZERO)) {
                        rateRadio.setSelected(true);
                    }
                    else {
                        interestsRadio.setSelected(true);
                    }
                }
                break;
        }
    }

    @FXML
    void clearInputs() {
        dateInput.setValue(null);
        amountInput.setText(null);
        descriptionInput.setText(null);
        selectedTransaction = TransactionDto.builder().build();
    }

    @FXML
    void saveTransaction() {
        if (inputValuesAreCorrect()){
            TransactionDto transactionDto = buildTransactionFromInputs();
            Task<Void> saveTask = new Task<Void>() {
                @Override
                protected Void call() {
                    transactionUIService.save(transactionDto);
                    return null;
                }
            };
            taskFactory.runTaskAndRecalculate(saveTask);
        }
    }

    @FXML
    void deleteSelectedTransaction() {
        TransactionDto toDelete = selectedTransaction;
        Task<Void> deleteTask = new Task<Void>() {
            @Override
            protected Void call(){
                transactionUIService.delete(toDelete);
                return null;
            }
        };
        taskFactory.runTaskAndRecalculate(deleteTask);
        selectedTransaction = TransactionDto.builder().build();
    }

    private TransactionDto buildTransactionFromInputs() {
        TransactionDto transactionDto = TransactionDto.builder()
                .id(selectedTransaction.getId())
                .date(dateInput.getValue())
                .description(descriptionInput.getText())
                .loanId(loanDetailsContainerController.getParentLoan().getId())
                .rate(NumberConverter.convertTextToRate(rateInput.getText()))
                .build();
        if (principalRadio.isSelected()) {
            transactionDto.setPrincipalChange(new BigDecimal(amountInput.getText()));
        }
        if (interestsRadio.isSelected()) {
            transactionDto.setInterestsChange(new BigDecimal(amountInput.getText()));
        }
        if (rateRadio.isSelected()) {
            transactionDto.setInterestsChange(BigDecimal.ZERO);
        }
        return transactionDto;
    }

    private boolean inputValuesAreCorrect() {
        return inputVerifier.inputValueIsNotNull(dateInput) &&
                inputVerifier.radioGroupHasOneSelected(radioGroup) &&
                inputVerifier.inputValueIsNotNull(rateInput) &&
                inputVerifier.inputValueIsCorrect(rateInput, () -> numberConverter.parseTextInputToDouble(rateInput)) &&
                (inputVerifier.inputValueIsNotNull(amountInput) || !amountInput.isVisible()) &&
                inputVerifier.inputValueIsCorrect(amountInput, () -> numberConverter.parseTextInputToBigDecimal(amountInput));
    }


}
