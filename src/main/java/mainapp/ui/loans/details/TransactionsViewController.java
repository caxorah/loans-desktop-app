package mainapp.ui.loans.details;

import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import lombok.RequiredArgsConstructor;
import mainapp.ui.commons.ViewLoader;
import mainapp.ui.customcells.AmountFormattedCell;
import mainapp.ui.customcells.PercentageFormattedCell;
import mainapp.ui.loans.LoanDto;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class TransactionsViewController {

    private final ViewLoader viewLoader;
    private final TransactionFormController transactionFormController;
    private final TransactionUIService transactionUIService;

    @FXML private BorderPane transactionsBorderPane;
    @FXML private TableView<TransactionDto> transactionsTable;
    @FXML private TableColumn<TransactionDto, LocalDate> colDate;
    @FXML private TableColumn<TransactionDto, String> colDescription;
    @FXML private TableColumn<TransactionDto, BigDecimal> colInterestsChange;
    @FXML private TableColumn<TransactionDto, BigDecimal> colPrincipalChange;
    @FXML private TableColumn<TransactionDto, Double> colRate;

    private LoanDto parentLoan;

    void setLoan(LoanDto loanDto) {
        this.parentLoan = loanDto;
        buildTableColumns();
        reloadTransactions();

        viewLoader.loadView("transactionForm",transactionsBorderPane.rightProperty());
        transactionFormController.setSelectedTransaction(TransactionDto.builder().build());
        transactionsTable.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (newSelection != null) transactionFormController.setSelectedTransaction(newSelection);
        });
    }

    private void buildTableColumns() {
        colDate.setCellValueFactory(new PropertyValueFactory<>("date"));
        colDescription.setCellValueFactory(new PropertyValueFactory<>("description"));
        colPrincipalChange.setCellValueFactory(new PropertyValueFactory<>("principalChange"));
        colInterestsChange.setCellValueFactory(new PropertyValueFactory<>("interestsChange"));
        colRate.setCellValueFactory(new PropertyValueFactory<>("rate"));

        colRate.setCellFactory(c -> new PercentageFormattedCell<>());
        colPrincipalChange.setCellFactory(c -> new AmountFormattedCell<>());
        colInterestsChange.setCellFactory(c -> new AmountFormattedCell<>());
    }

    public void reloadTransactions() {
        transactionUIService.setLoanId(parentLoan.getId());
        List<TransactionDto> transactionsList = transactionUIService.getAll().stream()
                .sorted(Comparator.comparing(TransactionDto::getDate))
                .collect(Collectors.toList());
        transactionsTable.setItems(FXCollections.observableList(transactionsList));
    }

}

