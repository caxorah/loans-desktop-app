package mainapp.ui.loans.details;

import mainapp.ui.commons.MyServices;
import mainapp.domain.transaction.Transaction;
import mainapp.domain.transaction.TransactionRepository;
import mainapp.domain.transaction.TransactionService;
import mainapp.ui.commons.PopAlert;
import mainapp.ui.company.CompanyUIService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class TransactionUIService extends TransactionService implements MyServices<TransactionDto> {

    final CompanyUIService companyUIService;
    private Long loanId;

    public TransactionUIService(TransactionRepository transactionRepository, CompanyUIService companyUIService) {
        super(transactionRepository);
        this.companyUIService = companyUIService;
    }

    private List<TransactionDto> getLoanTransactionsList(Long id) {
        List<Transaction> loanTransactions = this.getLoanTransactions(id);
        return loanTransactions.stream().map(TransactionDto::of).collect(Collectors.toList());
    }

    @Override
    public void save(TransactionDto transactionDto) {
        Transaction transaction = transactionDto.toDomain();
        super.save(transaction);
    }

    @PopAlert
    public void delete(TransactionDto transactionDto) {
        Long id = transactionDto.getId();
        super.delete(id);
    }

    @PopAlert
    public void deleteLoanTransactions() {
        super.deleteAllLoanTransactions(loanId);
    }

    @PopAlert
    @Override
    public List<TransactionDto> getAll() {
        return getLoanTransactionsList(loanId);
    }

    public void setLoanId(Long loanId) {
        this.loanId = loanId;
    }
}
