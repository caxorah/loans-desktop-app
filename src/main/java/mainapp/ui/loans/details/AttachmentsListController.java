package mainapp.ui.loans.details;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Cursor;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Callback;
import mainapp.domain.attachment.Attachment;
import mainapp.domain.attachment.AttachmentService;
import mainapp.ui.commons.TaskFactory;
import mainapp.ui.customcells.FileLinkCell;
import mainapp.ui.loans.LoanDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@Component
public class AttachmentsListController {

    private final AttachmentService attachmentService;
//    private LoanFormController loanFormController;

    @FXML private AnchorPane mainPane;
    @FXML private TableView<Attachment> attTable;
    @FXML private TableColumn<Attachment, String> colFile;
    @FXML private TableColumn<Attachment, String> colDescription;
    @FXML private TextField filePathInput;
    @FXML private TextField descriptionInput;

    private List<Attachment> tableContent;
    private File fileToUpload;
    private LoanDto parentLoan;

    @Autowired
    public AttachmentsListController(AttachmentService attachmentService) {
        this.attachmentService = attachmentService;
    }

//    @Autowired @Lazy
//    public void setLoanFormController(LoanFormController loanFormController) {
//        this.loanFormController = loanFormController;
//    }

    public void setLoan(LoanDto loanDto) {
        parentLoan = loanDto;
        refreshAttachmentsList();
        colFile.setCellValueFactory(new PropertyValueFactory<>("fileName"));
        colDescription.setCellValueFactory(new PropertyValueFactory<>("description"));
        Callback<TableColumn<Attachment, String>,
                TableCell<Attachment, String>> fileLinkCellFactory = (param) -> new FileLinkCell(attachmentService);
        colFile.setCellFactory(fileLinkCellFactory);
        colDescription.setCellFactory(TextFieldTableCell.forTableColumn());
        colDescription.setOnEditCommit(saveDescriptionChange);
    }

    private final EventHandler<TableColumn.CellEditEvent<Attachment,String>> saveDescriptionChange = (evt) -> {
        Attachment changedAttachment = evt.getRowValue();
        changedAttachment.setDescription(evt.getNewValue());
        Task<Void> saveChangeTask = new Task<Void>() {
            @Override
            protected Void call() {
                attachmentService.save(changedAttachment);
                return null;
            }
        };
        saveChangeTask.setOnRunning((e) -> mainPane.setCursor(Cursor.WAIT));
        saveChangeTask.setOnFailed((e) -> mainPane.setCursor(Cursor.DEFAULT));
        saveChangeTask.setOnSucceeded((e) -> {
            refreshAttachmentsList();
            mainPane.setCursor(Cursor.DEFAULT);
        });
        TaskFactory.runInDaemonThread(saveChangeTask);
    };


    @FXML
    void displayFileChooser() {
        FileChooser fileChooser = new FileChooser();
        //TODO automatically recognize operating system
//        String userSpecific = System.getProperty("user.name");
//        String initDirectory = "C:/Users/" + userSpecific + "/Downloads"; //Windows
        String initDirectory = "/home";   //Linux
        fileChooser.setInitialDirectory(new File(initDirectory));
        fileToUpload = fileChooser.showOpenDialog(getStage());
        if (fileToUpload!=null){
            filePathInput.setText(fileToUpload.getAbsolutePath());
        }
    }

    @FXML
    void saveFileToDb() {
        if (fileToUpload!=null) {
            Task<Void> saveAttachmentTask = new Task<Void>() {
                @Override
                protected Void call() {
                    attachmentService.saveFileToLoan(fileToUpload.getPath(),descriptionInput.getText(), parentLoan.getId());
                    return null;
                }
            };
            saveAttachmentTask.setOnRunning((e) -> mainPane.setCursor(Cursor.WAIT));
            saveAttachmentTask.setOnSucceeded((e) -> {
                refreshAttachmentsList();
                filePathInput.setText("");
                descriptionInput.setText("");
                mainPane.setCursor(Cursor.DEFAULT);
            });
            saveAttachmentTask.setOnFailed((e) -> mainPane.setCursor(Cursor.DEFAULT));
            TaskFactory.runInDaemonThread(saveAttachmentTask);
        }
    }

    @FXML
    void deleteSelectedFile() {
        Attachment selectedAttachment = attTable.getSelectionModel().getSelectedItem();
        Task<Void> deleteAttachmentTask = new Task<Void>() {
            @Override
            protected Void call() {
                attachmentService.delete(selectedAttachment);
                return null;
            }
        };
        deleteAttachmentTask.setOnRunning((e) -> mainPane.setCursor(Cursor.WAIT));
        deleteAttachmentTask.setOnFailed((e) -> mainPane.setCursor(Cursor.DEFAULT));
        deleteAttachmentTask.setOnSucceeded((e) -> {
            refreshAttachmentsList();
            mainPane.setCursor(Cursor.DEFAULT);
        });
        TaskFactory.runInDaemonThread(deleteAttachmentTask);
    }

    private Stage getStage(){
        return (Stage) mainPane.getScene().getWindow();
    }

    private void refreshAttachmentsList() {
        Task<Void> refreshListTask = new Task<Void>() {
            @Override
            protected Void call(){
                tableContent = attachmentService.getLoanAttachments(parentLoan.getId());
                ObservableList<Attachment> listOfAttachments = FXCollections.observableArrayList(tableContent);
                attTable.setItems(listOfAttachments);
                return null;
            }
        };
        refreshListTask.setOnRunning((e)-> mainPane.setCursor(Cursor.WAIT));
        refreshListTask.setOnFailed((e)-> {
            mainPane.setCursor(Cursor.DEFAULT);
            tableContent = new ArrayList<>();
        });
        refreshListTask.setOnSucceeded((e) -> mainPane.setCursor(Cursor.DEFAULT));
        TaskFactory.runInDaemonThread(refreshListTask);
    }

}
