package mainapp.ui.loans.details;

import javafx.fxml.FXML;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import lombok.RequiredArgsConstructor;
import mainapp.ui.commons.ViewLoader;
import mainapp.ui.loans.LoanDto;
import mainapp.ui.loans.LoanUIService;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class LoanDetailsContainerController {

    private final ViewLoader viewLoader;
    private final LoanFormController loanFormController;
    private final TransactionsViewController transactionsViewController;
    private final LoanBalancesViewController loanBalancesViewController;
    private final AttachmentsListController attachmentsListController;
    private final LoanUIService loanUIService;

    @FXML private AnchorPane mainPane;
    @FXML private BorderPane balancesPane;
    @FXML private BorderPane detailsPane;
    @FXML private BorderPane transactionsPane;
    @FXML private BorderPane attachmentsPane;


    private LoanDto parentLoan;


    public void setLoan(LoanDto loanDto) {
        this.parentLoan = loanDto;
        viewLoader.loadView("loanForm", detailsPane.centerProperty());
        loanFormController.setLoan(loanDto);
        viewLoader.loadView("transactionsView", transactionsPane.centerProperty());
        transactionsViewController.setLoan(loanDto);
        viewLoader.loadView("loanBalances", balancesPane.centerProperty());
        loanBalancesViewController.setLoan(loanDto);
        viewLoader.loadView("attachmentsList", attachmentsPane.centerProperty());
        attachmentsListController.setLoan(loanDto);
    }

    public void refreshLoan() {
        LoanDto refreshed = loanUIService.getLoan(parentLoan);
        setLoan(refreshed);
    }

    public AnchorPane getMainPane() {
        return mainPane;
    }

    public LoanDto getParentLoan() {
        return parentLoan;
    }
}
