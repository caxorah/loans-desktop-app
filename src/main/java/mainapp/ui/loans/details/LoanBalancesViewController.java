package mainapp.ui.loans.details;

import javafx.fxml.FXML;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import lombok.RequiredArgsConstructor;
import mainapp.domain.loan.Loan;
import mainapp.ui.commons.NumberConverter;
import mainapp.ui.company.CompanyUIService;
import mainapp.ui.loans.LoanDto;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class LoanBalancesViewController {

    private final CompanyUIService companyUIService;

    @FXML private DatePicker calcDateInput;
    @FXML private Label calcExpiredLabel;
    @FXML private Label calcInterestsBalanceLabel;
    @FXML private Label calcPrincipalBalanceLabel;
    @FXML private Label calcRepayStatusLabel;
    @FXML private Label calcTotalBalanceLabel;

    private LoanDto parentLoan;

    public void setLoan(LoanDto loanDto) {
        this.parentLoan = loanDto;
        this.calcDateInput.setValue(this.parentLoan.getStateDate());
        calculateStatusLabels();
    }

    @FXML
    void recalculateStatusFields() {
        Loan basicLoan = parentLoan.toDomain(companyUIService);
        this.parentLoan = new LoanDto(basicLoan,calcDateInput.getValue());
        calculateStatusLabels();
    }

    private void calculateStatusLabels() {
        calcExpiredLabel.setText(String.valueOf(parentLoan.isExpired()).toUpperCase());
        calcRepayStatusLabel.setText(parentLoan.getStatus().toString());
        calcPrincipalBalanceLabel.setText(NumberConverter.parseBigDecimalToFormattedString(parentLoan.getPrincipalBalance()));
        calcInterestsBalanceLabel.setText(NumberConverter.parseBigDecimalToFormattedString(parentLoan.getInterestsBalance()));
        calcTotalBalanceLabel.setText(NumberConverter.parseBigDecimalToFormattedString(parentLoan.getTotalLoanBalance()));
    }



}
