package mainapp.ui.loans;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.util.Callback;
import lombok.RequiredArgsConstructor;
import mainapp.domain.loan.LoanType;
import mainapp.domain.loan.RepaymentStatus;
import mainapp.ui.commons.MyUIController;
import mainapp.ui.commons.TaskFactory;
import mainapp.ui.commons.ViewLoader;
import mainapp.ui.customcells.AmountFormattedCell;
import mainapp.ui.customcells.CustomTrueFalseValueCell;
import mainapp.ui.customcells.LoanLinkCell;
import mainapp.ui.customcells.WrappingTextFieldTableCell;
import mainapp.ui.loans.details.LoanDetailsContainerController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.net.URL;
import java.time.LocalDate;
import java.util.List;
import java.util.ResourceBundle;
import java.util.function.Predicate;

@Component
@RequiredArgsConstructor
public class LoansViewController implements Initializable, MyUIController<LoanDto, AnchorPane> {

    private final LoanUIService loanService;
    private final ViewLoader viewLoader;
    private LoanDetailsContainerController loanDetailsContainerController;
    @Autowired
    @Lazy
    public void setLoanDetailsContainerController(LoanDetailsContainerController loanDetailsContainerController) {
        this.loanDetailsContainerController = loanDetailsContainerController;
    }

    @FXML private AnchorPane mainPane;
    @FXML private TableView<LoanDto> loansTable;
    @FXML private TableColumn<LoanDto, Long> colId;
    @FXML private TableColumn<LoanDto, String> colName;
    @FXML private TableColumn<LoanDto, Integer> colLender;
    @FXML private TableColumn<LoanDto, Integer> colBorrower;
    @FXML private TableColumn<LoanDto, LocalDate> colStartDate;
    @FXML private TableColumn<LoanDto, String> colLoanType;
    @FXML private TableColumn<LoanDto, BigDecimal> colAmount;
    @FXML private TableColumn<LoanDto, String> colCurrency;
    @FXML private TableColumn<LoanDto, RepaymentStatus> colStatus;
    @FXML private TableColumn<LoanDto, Boolean> colExpired;
    @FXML private TableColumn<LoanDto, String> colComment;
    @FXML private TableColumn<LoanDto, BigDecimal> colPrincipalBalance;
    @FXML private TableColumn<LoanDto, BigDecimal> colInterestsBalance;
    @FXML private TextField lenderFilterInput;
    @FXML private TextField borrowerFilterInput;
    @FXML private ComboBox<LoanType> loanTypeFilterCombo;

    private TaskFactory<LoanUIService, LoanDto, LoansViewController> taskFactory;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            taskFactory = new TaskFactory<>(loanService,this);
            buildColumnsValueFactory();
            taskFactory.runGetAllTask();
            loanTypeFilterCombo.setItems(FXCollections.observableArrayList(LoanType.values()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void buildColumnsValueFactory() {
        colId.setCellValueFactory(new PropertyValueFactory<>("id"));
        colName.setCellValueFactory(new PropertyValueFactory<>("name"));
        colLender.setCellValueFactory(new PropertyValueFactory<>("lender"));
        colBorrower.setCellValueFactory(new PropertyValueFactory<>("borrower"));
        colStartDate.setCellValueFactory(new PropertyValueFactory<>("startDate"));
        colAmount.setCellValueFactory(new PropertyValueFactory<>("initialPrincipalAmount"));
        colCurrency.setCellValueFactory(new PropertyValueFactory<>("currency"));
        colStatus.setCellValueFactory(new PropertyValueFactory<>("status"));
        colExpired.setCellValueFactory(new PropertyValueFactory<>("expired"));
        colComment.setCellValueFactory(new PropertyValueFactory<>("comment"));
        colPrincipalBalance.setCellValueFactory(new PropertyValueFactory<>("principalBalance"));
        colInterestsBalance.setCellValueFactory(new PropertyValueFactory<>("interestsBalance"));
        colLoanType.setCellValueFactory(new PropertyValueFactory<>("loanTypeName"));

        colAmount.setCellFactory(c -> new AmountFormattedCell<>());
        colPrincipalBalance.setCellFactory(c -> new AmountFormattedCell<>());
        colInterestsBalance.setCellFactory(c -> new AmountFormattedCell<>());
        colExpired.setCellFactory(c -> new CustomTrueFalseValueCell<>("Expired", null));
        colComment.setCellFactory(c -> new WrappingTextFieldTableCell<>());
        colComment.setOnEditCommit((evt)-> {
            LoanDto editedLoan = evt.getRowValue();
            editedLoan.setComment(evt.getNewValue());
            taskFactory.runSaveTask(editedLoan);
        });
        Callback<TableColumn<LoanDto, String>,
                TableCell<LoanDto, String>> loanLinkCellFactory = (param) -> new LoanLinkCell(this);
        colName.setCellFactory(loanLinkCellFactory);
    }

    @FXML
    void applyFilters() {
        taskFactory.runGetAllTask();
    }

    @FXML
    void clearFilters() {
        borrowerFilterInput.setText(null);
        lenderFilterInput.setText(null);
        loanTypeFilterCombo.setValue(null);
        taskFactory.runGetAllTask();
    }

    public void loadLoanDetailsForm(LoanDto loanDto) {
        viewLoader.loadView("loanDetailsContainer",((BorderPane)mainPane.getParent()).centerProperty());
        loanDetailsContainerController.setLoan(loanDto);
    }

    @Override
    public AnchorPane getMainPane() {
        return mainPane;
    }

    @Override
    public void refreshItemsList(List<LoanDto> list) {
        ObservableList<LoanDto> listOfLoans = FXCollections.observableList(list);
        FilteredList<LoanDto> filteredList = new FilteredList<>(listOfLoans, getCriteria());
        SortedList<LoanDto> sortedList = new SortedList<>(filteredList);
        sortedList.comparatorProperty().bind(loansTable.comparatorProperty());
        loansTable.setItems(sortedList);
    }

    private Predicate<LoanDto> getCriteria() {
        return loanDto ->
                lenderCriteriaMet(loanDto)
                && borrowerCriteriaMet(loanDto)
                && loanTypeCriteriaMet(loanDto);
    }

    private boolean lenderCriteriaMet(LoanDto loanDto){
        if (lenderFilterInput.getText()==null || lenderFilterInput.getText().equals("")) {
            return true;
        } else {
            return loanDto.getLender()==Integer.parseInt(lenderFilterInput.getText());
        }
    }

    private boolean borrowerCriteriaMet(LoanDto loanDto){
        if (borrowerFilterInput.getText()==null || borrowerFilterInput.getText().equals("")) {
            return true;
        } else {
            return loanDto.getBorrower()==Integer.parseInt(borrowerFilterInput.getText());
        }
    }

    private boolean loanTypeCriteriaMet(LoanDto loanDto) {
        if (loanTypeFilterCombo.getValue()==null) {
            return true;
        } else if (loanDto.getLoanType()==null) {
            return false;
        } else {
            return loanDto.getLoanType().equals(loanTypeFilterCombo.getValue());
        }
    }

}
