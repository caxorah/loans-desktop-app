package mainapp.ui.loans.interests;

import lombok.RequiredArgsConstructor;
import mainapp.domain.PeriodConverter;
import mainapp.domain.interests.LoanInterestsItem;
import mainapp.domain.loan.Loan;
import mainapp.domain.loan.RepaymentStatus;
import mainapp.ui.commons.PopAlert;
import mainapp.ui.loans.LoanUIService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.temporal.IsoFields;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Transactional
public class InterestsService {

    private final LoanUIService loanUIService;

    @PopAlert
    public List<LoanInterestsItemDto> getInterestsForPeriod(LocalDate startDate, LocalDate toDate) {
        List<Loan> allOpenLoans = loanUIService.getSummaryList().stream()
                .filter(l -> l.getRepaymentStatus(toDate).equals(RepaymentStatus.OPEN))
                .collect(Collectors.toList());
        return allOpenLoans.stream()
                .map(l -> new LoanInterestsItem(l,startDate,toDate,myPeriodConverter()))
                .map(LoanInterestsItemDto::new)
                .collect(Collectors.toList());
    }

    private PeriodConverter myPeriodConverter () {
        return date -> "Q" + date.plusMonths(2).get(IsoFields.QUARTER_OF_YEAR) + date.plusMonths(2).getYear();
    }

}
