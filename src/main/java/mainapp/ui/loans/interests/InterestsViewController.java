package mainapp.ui.loans.interests;

import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import lombok.RequiredArgsConstructor;
import mainapp.ui.commons.InputVerifier;
import mainapp.ui.customcells.AmountFormattedCell;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

@Component
@RequiredArgsConstructor
public class InterestsViewController implements Initializable {

    private final InputVerifier inputVerifier;
    private final InterestsService interestsService;

    @FXML private TableView<LoanInterestsItemDto> interestsTable;
    @FXML private TableColumn<LoanInterestsItemDto, String> colCurrency;
    @FXML private TableColumn<LoanInterestsItemDto, BigDecimal> colInterestsCalc;
    @FXML private TableColumn<LoanInterestsItemDto, String> colLoanName;

    @FXML private DatePicker dateFromInput;
    @FXML private DatePicker dateToInput;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        colLoanName.setCellValueFactory(new PropertyValueFactory<>("loanName"));
        colCurrency.setCellValueFactory(new PropertyValueFactory<>("loanCurrency"));
        colInterestsCalc.setCellValueFactory(new PropertyValueFactory<>("interestsCalculated"));
        colInterestsCalc.setCellFactory(c -> new AmountFormattedCell<>());
    }

    @FXML
    void getInterestsItemsList() {
        if (dateInputsAreCorrect()) {
            List<LoanInterestsItemDto> interestsForPeriod =
                    interestsService.getInterestsForPeriod(dateFromInput.getValue(), dateToInput.getValue());
            interestsTable.setItems(FXCollections.observableList(interestsForPeriod));
        }
    }

    private boolean dateInputsAreCorrect(){
        return inputVerifier.inputValueIsNotNull(dateFromInput) &&
                inputVerifier.inputValueIsNotNull(dateToInput) &&
                dateToInput.getValue().isAfter(dateFromInput.getValue());
    }



}
