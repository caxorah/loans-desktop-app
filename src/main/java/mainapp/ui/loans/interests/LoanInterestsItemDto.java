package mainapp.ui.loans.interests;

import lombok.Getter;
import mainapp.domain.interests.LoanInterestsItem;

import java.math.BigDecimal;

@Getter
public class LoanInterestsItemDto {

    private final String loanName;
    private final String loanCurrency;
    private final BigDecimal interestsCalculated;
    private final String description;

    public LoanInterestsItemDto(LoanInterestsItem item) {
        this.loanName = item.getLoan().getName();
        this.loanCurrency = item.getLoan().getCurrency().getCurrencyCode();
        this.interestsCalculated = item.getInterestsCalculatedForPeriod();
        this.description = item.getDescription();
    }

}
