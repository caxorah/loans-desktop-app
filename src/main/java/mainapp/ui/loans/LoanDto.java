package mainapp.ui.loans;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import mainapp.domain.interests.InterestsCalculationPolicy;
import mainapp.domain.loan.Loan;
import mainapp.domain.loan.LoanType;
import mainapp.domain.loan.RepaymentStatus;
import mainapp.domain.transaction.Transaction;
import mainapp.ui.company.CompanyUIService;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Currency;
import java.util.List;

@ToString
@Setter
@Getter
@NoArgsConstructor
public class LoanDto {

    private Long id;
    private String name;
    private Integer lender;
    private Integer borrower;
    private LocalDate startDate;
    private LocalDate endDate;
    private boolean autoExtend;
    private String currency;
    private InterestsCalculationPolicy interestsCalculationPolicy;
    private BigDecimal initialPrincipalAmount;
    private List<Transaction> transactions;
    private LoanType loanType;
    private String comment;

    private LocalDate stateDate;
    private BigDecimal principalBalance;
    private BigDecimal interestsBalance;
    private BigDecimal whtBalance;
    private boolean isExpired;
    private RepaymentStatus status;
    private BigDecimal totalLoanBalance;

    public LoanDto (Loan loan, LocalDate asOfDate ) {
        id = loan.getId();
        name = loan.getName();
        lender = loan.getLender().getEntityNumber();
        borrower = loan.getBorrower().getEntityNumber();
        startDate = loan.getStartDate();
        endDate = loan.getEndDate();
        autoExtend = loan.isAutoExtend();
        currency = loan.getCurrency().getCurrencyCode();
        interestsCalculationPolicy = loan.getInterestsCalculationPolicy();
        initialPrincipalAmount = loan.getLoanAmount();
        loanType = loan.getLoanType();
        stateDate = asOfDate;
        principalBalance = loan.getPrincipalBalance(asOfDate);
        interestsBalance = loan.getInterestsBalance(asOfDate);
        isExpired = loan.isExpired(asOfDate);
        status = loan.getRepaymentStatus(asOfDate);
        comment = loan.getComment();
        transactions = loan.getTransactions();
        totalLoanBalance = interestsBalance.add(principalBalance);
    }


    public Loan toDomain(CompanyUIService companyUIService) {
        return Loan.builder()
                .id(this.id)
                .lender(companyUIService.getCompanyByNumber(this.lender))
                .borrower(companyUIService.getCompanyByNumber(this.borrower))
                .startDate(this.startDate)
                .endDate(this.endDate)
                .autoExtend(this.autoExtend)
                .loanType(this.loanType)
                .loanAmount(this.initialPrincipalAmount)
                .currency(Currency.getInstance(this.currency))
                .interestsCalculationPolicy(this.interestsCalculationPolicy)
                .comment(this.comment)
                .name(this.name)
                .transactions(this.transactions)
                .build();
    }

    public String getLoanTypeName() {
        return this.loanType.name();
    }
}
