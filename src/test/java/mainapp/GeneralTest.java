package mainapp;


import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.ArchRule;
import com.tngtech.archunit.lang.CompositeArchRule;
import com.tngtech.archunit.library.GeneralCodingRules;
import org.springframework.context.annotation.Lazy;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

@AnalyzeClasses(packages = {"mainapp"})
public class GeneralTest {

    @ArchTest
    static final ArchRule find_printout =
            CompositeArchRule.of(GeneralCodingRules.NO_CLASSES_SHOULD_ACCESS_STANDARD_STREAMS);

    @ArchTest
    static final ArchRule find_lazy = noClasses().should().beAnnotatedWith(Lazy.class);
}
